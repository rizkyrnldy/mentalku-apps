// @flow

export default () => {
  const contentTheme = {
    flex: 1,
    backgroundColor: 'transparent',
    // marginBottom: 70,
    'NativeBase.Segment': {
      borderWidth: 0,
      backgroundColor: 'transparent'
    }
  };

  return contentTheme;
};
