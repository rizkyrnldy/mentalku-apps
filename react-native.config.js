module.exports = {
  project: {
    ios: {},
    android: {},
  },
  assets: [
    "./assets/fonts/FiraSans-Black.ttf",
    "./assets/fonts/FiraSans-BlackItalic.ttf",
    "./assets/fonts/FiraSans-Bold.ttf",
    "./assets/fonts/FiraSans-BoldItalic.ttf",
    "./assets/fonts/FiraSans-ExtraBold.ttf",
    "./assets/fonts/FiraSans-ExtraBoldItalic.ttf",
    "./assets/fonts/FiraSans-ExtraLight.ttf",
    "./assets/fonts/FiraSans-ExtraLightItalic.ttf",
    "./assets/fonts/FiraSans-Italic.ttf",
    "./assets/fonts/FiraSans-Light.ttf",
    "./assets/fonts/FiraSans-LightItalic.ttf",
    "./assets/fonts/FiraSans-Medium.ttf",
    "./assets/fonts/FiraSans-MediumItalic.ttf",
    "./assets/fonts/FiraSans-Regular.ttf",
    "./assets/fonts/FiraSans-SemiBold.ttf",
    "./assets/fonts/FiraSans-SemiBoldItalic.ttf",
    "./assets/fonts/FiraSans-Thin.ttf",
    "./assets/fonts/FiraSans-ThinItalic.ttf",
    
  ],
};