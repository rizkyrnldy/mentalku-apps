import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const unmoutListDataMaster = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_LIST_DATA_MASTER' })
}

export const listDataMaster = (type, successCB, failedCB) => async dispatch => {
  await dispatch({ "type": 'SET_LIST_DATA_MASTER' });
  const payload = { "type": type }
  return API.GET('/master', payload).then((res) => {
    const body = res.body;
    dispatch({ "type": 'SET_LIST_DATA_MASTER_SUCCESS', payload:{ 
      data: body,
    }});
    return successCB && successCB()
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(), dispatch({ "type": 'SET_LIST_DATA_MASTER_FAILED' }) ) 
    ));
  })
}

export const listDataMasterDomicile = (e, successCB, failedCB) => async (dispatch, getState) => {
  await dispatch({ "type": 'SET_LIST_DATA_MASTER' });
  const payload = { "type": 'domicile', "name": e }
  return API.GET('/master', payload).then((res) => {
    const body = res.body;
    dispatch({ "type": 'SET_LIST_DATA_MASTER_SUCCESS', payload:{ 
      data: body,
    }});
    return successCB && successCB()
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err.message), dispatch({  type: 'SET_LIST_DATA_MASTER_FAILED' }) ) : 
          dispatch(listDataMasterDomicile(e, successCB, failedCB))
    ));
  })
}


export const listDataMasterOutlet = (type, id, successCB, failedCB) => async dispatch => {
  await dispatch({ "type": 'SET_LIST_DATA_MASTER' });
  const payload = { "type": type, "city": id }
  return API.GET('/master', payload).then((res) => {
    const body = res.body;
    dispatch({ "type": 'SET_LIST_DATA_MASTER_SUCCESS', payload:{ 
      data: body,
    }});
    return successCB && successCB()
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      ( failedCB && failedCB(), dispatch({ "type": 'SET_LIST_DATA_MASTER_FAILED' }) ) 
    ));
  })
}

// export const listDataMasterCity = (type, id, successCB, failedCB) => async dispatch => {
//   await dispatch({ "type": 'SET_LIST_DATA_MASTER_CITY' });
//   const payload = { "type": type, "id": id }
//   return API.GET('/datamaster', payload).then((res) => {
//     const body = res.data.body;
//     dispatch({ "type": 'SET_LIST_DATA_MASTER_CITY_SUCCESS', payload:{ 
//       data: body,
//     }});
//     return successCB && successCB()
//   }).catch((err) => {
//     return dispatch(errorHandler(
//       err, 
//       ( failedCB && failedCB(), dispatch({ "type": 'SET_LIST_DATA_MASTER_CITY_FAILED' }) ) 
//     ));
//   })
// }
