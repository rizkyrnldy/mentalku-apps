import AsyncStorage from '@react-native-async-storage/async-storage';
import { API } from '../../../../config';
import { errorHandler } from '../../errorAction';
import update from 'react-addons-update';

export const startQuiz = (value, successCB, failedCB) => async dispatch => {  
  return API.POST('/quiz/start', value).then((res) => {
    var status = res.status;
    return successCB && successCB(status);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err) ) : 
          dispatch(startQuiz(value, successCB, failedCB))
    ));
  })

}

export const setListQuiz = (value, successCB, failedCB) => async dispatch => {  
  await dispatch({ type: 'SET_QUIZ' })
  return API.GET('/quiz', value).then((res) => {
    var body = res.body;
    dispatch({ type: 'SET_QUIZ_SUCCESS', payload: {
      data: body
    } })
    return successCB && successCB(res);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err), dispatch({ type: 'SET_QUIZ_FAILED' }) ) : 
          dispatch(setListQuiz(value, successCB, failedCB))
    ));
  })
}


export const setAnswerQuiz = (value, successCB, failedCB) => async dispatch => {  
  var answerQuiz = await AsyncStorage.getItem('answerQuiz');
  // console.log(222, answerQuiz.length);
  var dataParsing = answerQuiz ? JSON.parse(answerQuiz) : {
    "current": 0,
    "question": []
  }
  var newParsing = update(dataParsing, {
    current: {$set: dataParsing.current + 1},
    question: {$push: [value]}
  })
  await dataParsing.current <= 30 && AsyncStorage.setItem('answerQuiz', JSON.stringify(newParsing));
  // await dataParsing.current <= 2 && AsyncStorage.setItem('answerQuiz', JSON.stringify(newParsing));
  return successCB && successCB(newParsing);
 
}


export const sendResultQuiz = (value, successCB, failedCB) => async dispatch => {  
  // await dispatch({ type: 'SET_QUIZ' })
  return API.POST('/result', value).then((res) => {
    var body = res.body;
    AsyncStorage.removeItem('answerQuiz');
    return successCB && successCB(body);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err) ) : 
          dispatch(sendResultQuiz(value, successCB, failedCB))
    ));
  })
}




