import { API } from '../../../../config';
import { errorHandler } from '../../errorAction';

export const unmountListPsikolog = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_LIST_PSIKOLOG' })
}

export const unmountDetailPsikolog = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_DETAIL_PSIKOLOG' })
}

export const unmountSimTest = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_SET_SIM' })
}


export const setListPsikolog = (search, current, successCB, failedCB) => async (dispatch) => {
  // await dispatch({ type: 'SET_LIST_PSIKOLOG' })
  var payload = {
      "userType": 3,
      "current": current, 
      "filter":{ 
        "type": "fullName", 
        "search": search, 
        "orderBy": "desc" 
      } 
  }
  return API.GET('/users', payload).then((res)  =>  {
    dispatch({
      type: 'SET_LIST_PSIKOLOG_SUCCESS',
      payload: {
        data: res.body.data,
      }
    })
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err), dispatch({  type: 'SET_LIST_PSIKOLOG_FAILED' }) ) : 
          dispatch(setListPsikolog(search, current, successCB, failedCB))
    ));
  })
}

export const setDetailPsikolog = (uid, successCB, failedCB) => async (dispatch) => {
  var payload = { uid }
  await dispatch({ type: 'SET_DETAIL_PSIKOLOG' });
  return API.GET('/users/detail', payload).then((res)  =>  {
    dispatch({
      type: 'SET_DETAIL_PSIKOLOG_SUCCESS',
      payload: {
        data: res.body,
      }
    })
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err), dispatch({  type: 'SET_DETAIL_PSIKOLOG_FAILED' }) ) : 
          dispatch(setDetailPsikolog(uid, successCB, failedCB))
    ));

  })
}

export const setBookingPsikolog = (payload, successCB, failedCB) => async (dispatch) => {
  return API.POST('/counseling/create', payload).then((res)  =>  {
    return successCB && successCB(res);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err)) : 
          dispatch(setBookingPsikolog(payload, successCB, failedCB))
    ));
  })

}


