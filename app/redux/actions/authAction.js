import AsyncStorage from '@react-native-async-storage/async-storage';
import { API } from '../../config';
import { errorHandler } from './errorAction';
var jwtDecode = require('jwt-decode');

export const checkAuth = () => async dispatch => {
  await dispatch({type: 'LOAD_AUTH'});
  const loginUser = await AsyncStorage.getItem('tokenUser');
  const refreshToken = await AsyncStorage.getItem('refreshTokenUser');
  if(loginUser){
    const dataToken = jwtDecode(loginUser);
    return dispatch({
      type: 'LOAD_AUTH_SUCCESS',
      payload: {
        authed: true,
        dataUser: dataToken.data,
        token: loginUser,
        refreshToken: refreshToken,
      }
    });
  }else{
    return dispatch({ type: 'LOAD_AUTH_FAILED' });
  }
}

export const refreshToken = () => async dispatch => {
  var refreshToken = await AsyncStorage.getItem('refreshTokenUser')
  var payload = { "refreshToken": refreshToken }
  return API.POST('/refresh/token', payload).then((res) => {
    const body = res.body;
    const dataToken = jwtDecode(body.token);
    AsyncStorage.setItem('tokenUser', body.token);
    AsyncStorage.setItem('refreshTokenUser', body.refreshToken);
    return dispatch({ 
      type: 'LOAD_AUTH_SUCCESS', 
      payload: {  
        authed: true,
        dataUser: dataToken.data,
        token: body.token,
        refreshToken: body.refreshToken,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err), dispatch({ type: 'LOAD_AUTH_FAILED' }) ): 
          dispatch(refreshToken())
    ));
  })
}

export const reqLogin = (data, successCB, failedCB) => async () => {
  return API.POST('/signup/apps', data).then((res)  =>  {
    var vcid = res.body.vcid;
    return successCB && successCB(vcid);
  }).catch((err) => {
    return failedCB && failedCB(err.message);
  });
}

export const setVerification = (data, successCB, failedCB) => async dispatch => {
  return API.POST('/verification', data).then((res)  =>  {
    const body = res.body;
    const dataToken = jwtDecode(body.token);
    AsyncStorage.setItem('tokenUser', body.token);
    AsyncStorage.setItem('refreshTokenUser', body.refreshToken);
    return (
      dispatch({
        type: 'LOAD_AUTH_SUCCESS',
        payload: {
          authed: true,
          dataUser: dataToken.data,
          token: body.token,
          refreshToken: body.refreshToken,
        }
      }),
      successCB && successCB()
    )
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err) ): 
          dispatch(setVerification(data, successCB, failedCB))
    ));
  })
}

export const resendVerification = (data, successCB, failedCB) => async dispatch => {
  return API.POST('/resend/verification', data).then((res)  =>  {
    return successCB && successCB(res)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err) ): 
          dispatch(resendVerification(data, successCB, failedCB))
    ));
  })
}

export const setLogout = (successCB, failedCB) => async dispatch => {
  await dispatch({type: 'LOGOUT'});
  AsyncStorage.removeItem('tokenUser');
  AsyncStorage.removeItem('refreshTokenUser');
  return API.GET('/logout').then(()  =>  {
    successCB && successCB()
    return dispatch({ type: 'LOGOUT_SUCCESS' });
  }).catch(() => {
    dispatch({ type: 'LOGOUT_FAILED' });
    return failedCB && failedCB()
  })
}


export const checkEmail = (email, successCB, failedCB) => async dispatch => {
  var payload = { email: email }
  return API.POST('/check/email', payload).then((res)  =>  {
    return successCB && successCB() 
  }).catch((err) => {
    return failedCB && failedCB('err.message')
  })
}

export const changeToken = (body, cb) => async (dispatch, getState) => {
  if(body.token){
    const dataToken = jwtDecode(body.token);
    AsyncStorage.setItem('tokenUser', body.token);
    AsyncStorage.setItem('refreshTokenUser', body.refreshToken);
    return (
      dispatch({
        type: 'LOAD_AUTH_SUCCESS',
        payload: {
          authed: true,
          dataUser: dataToken.data,
          token: body.token,
          refreshToken: body.refreshToken,
        }
      }),
      cb()
    )
  }else{
    return cb();
  }
}


export const registerFCM = (value, successCB, failedCB) => async (dispatch, getState) => {
  if(value.fcm){
    return API.POST('/fcm', value).then(()  =>  {
      return successCB && successCB() 
    }).catch((err) => {
      return dispatch(errorHandler(
        err, 
        (cb) => cb === 'FAILED' ? 
          failedCB && failedCB(err.message) : 
            dispatch(registerFCM(value, successCB, failedCB))
      ));
    })
  }else{
    return successCB && successCB();
  }

}
