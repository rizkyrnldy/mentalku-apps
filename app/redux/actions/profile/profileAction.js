import AsyncStorage from '@react-native-async-storage/async-storage';
import { API } from '../../../config';
import { errorHandler } from '../errorAction';
import { changeToken } from '../authAction';
var jwtDecode = require('jwt-decode');

export const unmountProfile = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_PROFILE'})
}

export const setProfile = () => async dispatch => {
  await dispatch({type: 'SET_PROFILE'});
  return API.GET('/users/detail').then((res) => {
    const body = res.body;
    return dispatch({ 
      type: 'SET_PROFILE_SUCCESS', 
      payload: {  
        data: body,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err.message), dispatch({  type: 'SET_PROFILE_FAILED' }) ) : 
          dispatch(setProfile())
    ));
  })
}

// export const editProfileCredential = (value, successCB, failedCB) => async dispatch => {
//   return API.POST('/email', value).then((res) => {
//     const body = res.body;
//     return successCB && successCB(body);
//   }).catch((err) => {
//     return dispatch(errorHandler(
//       err, 
//       (cb) => cb === 'FAILED' ? 
//         ( failedCB && failedCB(err.message) ) : 
//           dispatch(editProfileCredential(value, successCB, failedCB))
//     ));
//   })
// }

// export const verificationEditProfile = (value, successCB, failedCB) => async dispatch => {
//   return API.POST('/users/edit', value).then((res) => {
//     const body = res.body;
//     return (
//       dispatch({ 
//         type: 'SET_PROFILE_SUCCESS', 
//         payload: {  
//           data: body,
//         }
//       }),
//       dispatch(changeToken(body, () => {
//         return successCB && successCB(body);
//       }))
//     );
//   }).catch((err) => {
//     return dispatch(errorHandler(
//       err, 
//       (cb) => cb === 'FAILED' ? 
//         (failedCB && failedCB(err.message), dispatch({  type: 'SET_PROFILE_FAILED' }) ) : 
//           dispatch(verificationEditProfile(value, successCB, failedCB))
//     ));
//   })
// }

// export const resendVerificationEditProfile = (value, successCB, failedCB) => async dispatch => {
//   return API.POST('/users/edit', value).then((res) => {
//     const body = res.body;
//     return (
//       dispatch({ 
//         type: 'SET_PROFILE_SUCCESS', 
//         payload: {  
//           data: body,
//         }
//       }),
//       dispatch(changeToken(body, () => {
//         return successCB && successCB(body);
//       }))
//     );
//   }).catch((err) => {
//     return dispatch(errorHandler(
//       err, 
//       (cb) => cb === 'FAILED' ? 
//         (failedCB && failedCB(err.message), dispatch({  type: 'SET_PROFILE_FAILED' }) ) : 
//           dispatch(resendVerificationEditProfile(value, successCB, failedCB))
//     ));
//   })
// }

export const editProfileInformation = (value, successCB, failedCB) => async dispatch => {
  return API.UPLOAD('/users/edit', value).then((res) => {
    const body = res.body;
    return (
      dispatch({ 
        type: 'SET_PROFILE_SUCCESS', 
        payload: {  
          data: body,
        }
      }),
      dispatch(changeToken(body, () => {
        return successCB && successCB(body);
      }))
    );
  }).catch((err) => {
    console.log(8888, err);
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        // (failedCB && failedCB(err.message), dispatch({  type: 'SET_PROFILE_FAILED' }) ) : 
        (failedCB && failedCB(err.message)) : 
          dispatch(editProfileInformation(value, successCB, failedCB))
    ));
  })
}