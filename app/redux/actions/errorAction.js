import { setLogout, refreshToken } from './authAction';
import { ToastAndroid } from 'react-native';

export const errorHandler = (err, cb) => async dispatch => {
  // console.log(91913, err);
  if(err){
    // console.log(9191, err);
    // ToastAndroid.show(`${err.message}`, ToastAndroid.SHORT);
    switch (err.shortCode) {
      case "Error":
      case "UNAUTHORIZED":
      case "INVALID_TOKEN":
        return dispatch(setLogout())
      case "SESSION_EXPIRED":
        await dispatch(refreshToken());
        return cb()
      case "FAILED":
        return cb("FAILED")
      default:
        return null
    }  
  }else{
    // return dispatch(setLogout())
  }
}
