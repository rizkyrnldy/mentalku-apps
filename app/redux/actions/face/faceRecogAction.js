import { API } from '../../../config';
import { errorHandler } from '../errorAction';
import { changeToken } from '../authAction';

export const registerFace = (payload, successCB, failedCB) => async (dispatch, getState) => {
  return API.POST('/face/register', payload).then((res) => {
    var status = res.status;
    const body = res.body;
    dispatch({ 
      type: 'SET_PROFILE_SUCCESS', 
      payload: {  
        data: body,
      }
    }),
    dispatch(changeToken(body, () => {
      return successCB && successCB(status);
    }))
    // return successCB && successCB(status);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err) ) : 
          dispatch(registerFace(payload, successCB, failedCB))
    ));
  })
}

export const checkFace = (payload, successCB, failedCB) => async (dispatch, getState) => {
  return API.FACE('/recognize', payload).then((res) => {
    var body = res.body;
    var status = res.status;
    return successCB && successCB(status);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err) ) : 
          dispatch(checkFace(payload, successCB, failedCB))
    ));
  })
}
