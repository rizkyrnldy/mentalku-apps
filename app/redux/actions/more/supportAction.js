import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const setSupport = (data, successCB, failedCB) => async dispatch => {
  return API.POST('/support/create', data).then((res)  =>  {
    return successCB && successCB(res)
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? (failedCB && failedCB(err)) : dispatch(setSupport(data, successCB, failedCB))
    ));
  });
}