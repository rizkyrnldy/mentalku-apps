import { API } from '../../../config';
import { errorHandler } from '../errorAction';
import { b64toBlob, documentView } from '../../../src/helper';


export const unmountListCertificate = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_CERTIFICATE_LIST'})
}
export const unmountDetailCertificate = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_CERTIFICATE_DETAIL'})
}

export const setListCertificate = (payload) => async dispatch => {
  await dispatch({type: 'SET_CERTIFICATE'});
  return API.GET('/certificate', payload).then((res) => {
    const body = res.body.data;
    return dispatch({ 
      type: 'SET_CERTIFICATE_LIST_SUCCESS', 
      payload: {  
        data: body,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_CERTIFICATE_LIST_FAILED' }) : 
          dispatch(setListCertificate(value))
    ));
  })
}

export const setListCertificateMore = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/certificate', value).then((res) => {
    const body = getState().more.certificate.list.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_CERTIFICATE_LIST_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_CERTIFICATE_LIST_FAILED' }) : 
          dispatch(setListCertificateMore(value, successCB))
    ));
  })
}

export const setDetailCertificate = (payload) => async dispatch => {
  await dispatch({type: 'SET_CERTIFICATE_DETAIL'});
  return API.POST('/document', payload).then((res) => {
    const body = res.body;
    return dispatch({ 
      type: 'SET_CERTIFICATE_DETAIL_SUCCESS', 
      payload: {  
        data: `data:application/pdf;base64,${body}`,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_CERTIFICATE_DETAIL_FAILED' }) : 
          dispatch(setDetailCertificate(payload))
    ));
  })
}