import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const unmountTerms = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_TERMS'})
}

export const setTerms = () => async dispatch => {
  await dispatch({type: 'SET_TERMS'});
  var payload = { id: 3 }
  return API.GET('/term/detail', payload).then((res) => {
    const body = res.body;
    return dispatch({ 
      type: 'SET_TERMS_SUCCESS', 
      payload: {  
        data: body,
      }
    });
  }).catch(() => {
    return dispatch({  type: 'SET_TERMS_FAILED' });
  })
}
