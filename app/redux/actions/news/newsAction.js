import { API } from '../../../config';
import { errorHandler } from '../errorAction';
var qs = require('qs');

export const unmountListNews = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_NEWS'})
}
export const unmountDetailNews = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_DETAIL_NEWS'})
}

export const setListNews = (value) => async dispatch => {
  await dispatch({type: 'SET_LIST_NEWS'});
  var query = value ? qs.stringify(value, { encode: false }) : null;
  return API.WP(`/posts?${query}`).then((res) => {
    const body = res.data;
    return dispatch({ 
      type: 'SET_LIST_NEWS_SUCCESS', 
      payload: {  
        data: body,
      }
    });
  }).catch(() => {
    return dispatch({  type: 'SET_LIST_NEWS_FAILED' });
  })
}

export const setLoadMore = (value, successCB, failedCB) => async dispatch => {
  var query = value ? qs.stringify(value, { encode: false }) : null;
  return API.WP(`/posts?${query}`).then((res) => {
    const body = res.data;
    dispatch({ 
      type: 'SET_LIST_NEWS_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch(() => {
    dispatch({  type: 'SET_LIST_NEWS_FAILED' });
    return failedCB && failedCB();
  })
}


export const setDetailNews = (id, value, successCB, failedCB) => async dispatch => {
  if(id === null){
    await dispatch({
      type: 'SET_DETAIL_NEWS_SUCCESS',
      payload: {
        data: value
      }
    });
    return successCB && successCB();
  }else{
    await dispatch(
      setDetailNewsFetch(id, () => {
        return successCB && successCB()
      }, () => {
        return failedCB && failedCB()
      })
    )
  }
}

export const setDetailNewsFetch = (id, successCB, failedCB) => async dispatch => {
  await dispatch({type: 'SET_DETAIL_NEWS'});
  return API.WP(`/posts/${id}`).then((res) => {
    const body = res.data;
    dispatch({ 
      type: 'SET_DETAIL_NEWS_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB()
  }).catch(() => {
    dispatch({  type: 'SET_DETAIL_NEWS_FAILED' });
    return failedCB && failedCB()
  })
}

