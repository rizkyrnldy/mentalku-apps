import { API } from '../../../config';
import { errorHandler } from '../errorAction';
import moment from 'moment';

export const unmountListBilling = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_BILLING_PAYMENT'})
}

export const setListBilling = (payload) => async dispatch => {
  await dispatch({type: 'SET_LIST_BILLING'});
  return API.GET('/notif', payload).then((res) => {
    const body = res.body.data;
    return dispatch({ 
      type: 'SET_LIST_BILLING_PAYMENT_SUCCESS', 
      payload: {  
        data: body,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_BILLING_PAYMENT_FAILED' }) : 
          dispatch(setListBilling(payload))
    ));
  })
}

export const setLoadMoreBilling = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/notif', value).then((res) => {
    const body = getState().billing.list.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_LIST_BILLING_PAYMENT_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_BILLING_PAYMENT_FAILED' }) : 
          dispatch(setLoadMoreBilling(value, successCB))
    ));
  })
}

// export const setBillingMerge = (value) => async (dispatch, getState) => {
//   var newData = {
//     title: value.request.content.title,
//     message: value.request.content.body,
//     created_at: moment(new Date()).format('YYYY-MM-DD H:m:s'),
//   } 
//   var body = getState().billing.list.data
//   body.unshift(newData);
//   return dispatch({ 
//     type: 'SET_LIST_BILLING_PAYMENT_SUCCESS', 
//     payload: {  
//       data: body
//     }
//   });
// }

