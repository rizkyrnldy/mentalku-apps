import { API } from '../../../config';
import { errorHandler } from '../errorAction';
import moment from 'moment';

export const unmountListNotification = () => async dispatch => {
  return dispatch({type: 'UNMOUNT_LIST_NOTIFICATION'})
}

export const setListNotification = (value) => async dispatch => {
  await dispatch({type: 'SET_LIST_NOTIFICATION'});
  return API.GET('/notif', value).then((res) => {
    const body = res.body.data;
    return dispatch({ 
      type: 'SET_LIST_NOTIFICATION_SUCCESS', 
      payload: {  
        data: body,
      }
    });
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_NOTIFICATION_FAILED' }) : 
          dispatch(setListNotification(value))
    ));
  })
}

export const setLoadMoreNotification = (value, successCB) => async (dispatch, getState) => {
  return API.GET('/notif', value).then((res) => {
    const body = getState().notification.list.data.concat(res.body.data)
    dispatch({ 
      type: 'SET_LIST_NOTIFICATION_SUCCESS', 
      payload: {  
        data: body,
      }
    });
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        dispatch({  type: 'SET_LIST_NOTIFICATION_FAILED' }) : 
          dispatch(setLoadMoreNotification(value, successCB))
    ));
  })

}

export const setNotificationMerge = (value) => async (dispatch, getState) => {
  var newData = {
    title: value.request.content.title,
    message: value.request.content.body,
    created_at: moment(new Date()).format('YYYY-MM-DD H:m:s'),
  } 
  var body = value.request.content.data.type === 'NOTIFICATION' ? getState().notification.list.data : getState().billing.list.data;
  body.unshift(newData);
  var _type = `SET_LIST_${value.request.content.data.type}_SUCCESS`;
  return dispatch({ 
    type: _type, 
    payload: {  
      data: body
    }
  });
}

