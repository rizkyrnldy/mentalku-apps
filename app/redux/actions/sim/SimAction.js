import { API } from '../../../config';
import { errorHandler } from '../errorAction';

export const unmountSimTest = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_SET_SIM' })
}
export const unmountSimOutlet = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_SET_SIM_OUTLET' })
}
export const unmountSimSubmit = () => async dispatch => {
  return dispatch({ type: 'UNMOUNT_SET_SIM_SUBMIT' })
}

export const checkSimTest = (successCB, failedCB) => async dispatch => {  
  await dispatch({ type: 'SET_SIM' })
  return API.GET('/sim/check').then((res)  =>  {
    dispatch({
      type: 'SET_SIM_SUCCESS',
      payload: {
        // data: res.body.code ? res.body.code : res.body ? res.body : null,
        data: res.body ? res.body.code ? res.body.code : res.body : null,
        status: res.status.shortCode
      }
    })
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err.message), dispatch({  type: 'SET_SIM_FAILED' }) ) : 
          dispatch(checkSimTest(successCB, failedCB))
    ));
  })
}


export const checQuotaSim = (admId, successCB, failedCB) => async (dispatch, getState) => {
  var payload ={ uid: admId };
  return API.GET('/sim/check/quota', payload).then((res)  =>  {
    var body = res.body;
    return successCB && successCB(body);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        failedCB && failedCB(err.message) : 
          dispatch(checQuotaSim(admId, successCB, failedCB))
    ));
  })
}

export const setOutlet = (value, successCB, failedCB) => async (dispatch, getState) => {
  // await dispatch({ type: 'SET_SIM_OUTLET' })
  var payload = { 
    "userType": 2, 
    "current": 1, 
    "filter":{ 
      "type": "domicile", 
      "search": value, 
      "orderBy": "asc" 
    } 
  };
  return API.GET('/users', payload).then((res)  =>  {
    var body = res.body;
    dispatch({ 
      type: 'SET_SIM_OUTLET_SUCCESS',
      payload: {
        list: body
      }
    })
    return successCB && successCB(body);
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        ( failedCB && failedCB(err.message), dispatch({ type: 'SET_SIM_OUTLET_FAILED' }) ) : 
          dispatch(setOutlet(value, successCB, failedCB))
    ));
  })
}

export const setOutletDetail = (res, successCB, failedCB) => async (dispatch, getState) => {
  await dispatch({
    type: 'SET_SIM_DETAIL_OUTLET_SUCCESS',
    payload: {
      detail: res
    }
  })
  return successCB && successCB();
}

export const setSimSubmit = (res, successCB, failedCB) => async (dispatch, getState) => {
  var payload  = {
    uid: getState().sim.outlet.detail.user_contacts.userId,
    category: res.category.split(', ')[0],
    type: res.type,
    price: res.category.split(', ')[1],
  }

  await dispatch({
    type: 'SET_SIM_SUBMIT_SUCCESS',
    payload: {
      data: payload
    }
  })
  return successCB && successCB();
}


export const createSimTest = (successCB, failedCB) => async (dispatch, getState) => {
  await dispatch({ type: 'SET_SIM' })
  const val = getState().sim.submit.data;
  return API.POST('/sim/create', val).then((res)  =>  {
    console.log(91283, res);
    const body = res.body;
    dispatch({
      type: 'SET_SIM_SUCCESS',
      payload: {
        data: body,
        status: 'TEST_IS_NOT_READY'
      }
    })
    return successCB && successCB();
  }).catch((err) => {
    return dispatch(errorHandler(
      err, 
      (cb) => cb === 'FAILED' ? 
        (failedCB && failedCB(err.message), dispatch({  type: 'SET_SIM_FAILED', payload: { status: 'USER_NOT_REGISTER' } }) ) : 
          dispatch(createSimTest(successCB, failedCB))
    ));
  })
}