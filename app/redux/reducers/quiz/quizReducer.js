const SET_QUIZ = 'SET_QUIZ';
const SET_QUIZ_SUCCESS = 'SET_QUIZ_SUCCESS';
const SET_QUIZ_FAILED = 'SET_QUIZ_FAILED';
const UNMOUNT_SET_QUIZ = 'UNMOUNT_SET_QUIZ';

const initialState = {
  loading: true,
  data: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_QUIZ:
      return { 
        ...state, 
        loading: true,
      }
    case SET_QUIZ_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
      }
    case SET_QUIZ_FAILED:
      return { 
        ...state, 
        loading: false,
        data: null,
      }
    case UNMOUNT_SET_QUIZ:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
