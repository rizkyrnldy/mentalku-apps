const SET_LIST_BILLING_PAYMENT = 'SET_LIST_BILLING_PAYMENT';
const SET_LIST_BILLING_PAYMENT_SUCCESS = 'SET_LIST_BILLING_PAYMENT_SUCCESS';
const SET_LIST_BILLING_PAYMENT_FAILED = 'SET_LIST_BILLING_PAYMENT_FAILED';
const UNMOUNT_LIST_BILLING_PAYMENT = 'UNMOUNT_LIST_BILLING_PAYMENT';

const initialState = {
  loading: true,
  data: [],
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case SET_LIST_BILLING_PAYMENT:
      return {
        ...state,
        loading: true
      };
    case SET_LIST_BILLING_PAYMENT_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case SET_LIST_BILLING_PAYMENT_FAILED:
      return {
        ...state,
        loading: true,
        data: [],
      };
    case UNMOUNT_LIST_BILLING_PAYMENT:
      return {
        ...state,
        loading: true,
        data: [],
      };
    default:
      return state;
  }
}
