const SET_LIST_NEWS = 'SET_LIST_NEWS';
const SET_LIST_NEWS_SUCCESS = 'SET_LIST_NEWS_SUCCESS';
const SET_LIST_NEWS_FAILED = 'SET_LIST_NEWS_FAILED';
const UNMOUNT_LIST_NEWS = 'UNMOUNT_LIST_NEWS';

const initialState = {
  loading: true,
  data: null,
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case SET_LIST_NEWS:
      return {
        ...state,
        loading: true
      };
    case SET_LIST_NEWS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case SET_LIST_NEWS_FAILED:
      return {
        ...state,
        loading: true,
        data: null,
      };
    case UNMOUNT_LIST_NEWS:
      return {
        ...state,
        loading: true,
        data: null,
      };
    default:
      return state;
  }
}
