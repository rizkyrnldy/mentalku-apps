const SET_DETAIL_NEWS = 'SET_DETAIL_NEWS';
const SET_DETAIL_NEWS_SUCCESS = 'SET_DETAIL_NEWS_SUCCESS';
const SET_DETAIL_NEWS_FAILED = 'SET_DETAIL_NEWS_FAILED';
const UNMOUNT_DETAIL_NEWS = 'UNMOUNT_DETAIL_NEWS';

const initialState = {
  loading: true,
  data: null,
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case SET_DETAIL_NEWS:
      return {
        ...state,
        loading: true
      };
    case SET_DETAIL_NEWS_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case SET_DETAIL_NEWS_FAILED:
      return {
        ...state,
        loading: true,
        data: null,
      };
    case UNMOUNT_DETAIL_NEWS:
      return {
        ...state,
        loading: true,
        data: null,
      };
    default:
      return state;
  }
}
