import { combineReducers } from 'redux';
import auth from './authReducer';
import signupCredential from './signup/SignupCredentialReducer';
import profile from './profile/ProfileReducer';
import newsList from './news/NewsListReducer';
import newsDetail from './news/NewsDetailReducer';
import sim from './sim/simReducer';
import simOutlet from './sim/simOutletReducer';
import simSubmit from './sim/simSubmitReducer';
import quiz from './quiz/quizReducer';
import listPsikolog from './test/konseling/listPsikologReducer';
import detailPsikolog from './test/konseling/detailPsikologReducer';
import dataMaster from './component/dataMasterReducer';
import listNotification from './notification/notificationListReducer';
import listBilling from './billing/billingListReducer';
import termCondition from './more/termConditionReducer';
import listCertificate from './more/certificate/certificateListReducer';
import detailCertificate from './more/certificate/certificateDetailReducer';

export default combineReducers({
  auth,
  signup: combineReducers({
    credential: signupCredential
  }),
  profile,
  news: combineReducers({
    list: newsList,
    detail: newsDetail
  }),
  sim: combineReducers({
    sim: sim,
    outlet: simOutlet,
    submit: simSubmit,
  }),
  test: combineReducers({
    konseling: combineReducers({
      list: listPsikolog,
      detail: detailPsikolog
    }),
  }),
  quiz: combineReducers({
    quiz: quiz
  }),
  component: combineReducers({
    dataMaster
  }),
  notification: combineReducers({
    list: listNotification
  }),
  
  billing: combineReducers({
    list: listBilling
  }),

  more: combineReducers({
    termCondition,
    certificate: combineReducers({
      list: listCertificate,
      detail: detailCertificate
    })
  }),
});

