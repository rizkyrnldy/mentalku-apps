const SET_SIM_SUBMIT_SUCCESS = 'SET_SIM_SUBMIT_SUCCESS';
const UNMOUNT_SET_SIM_SUBMIT = 'UNMOUNT_SET_SIM_SUBMIT';

const initialState = {
  data: null
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_SIM_SUBMIT_SUCCESS:
      return { 
        ...state, 
        data: payload.data
      }
    case UNMOUNT_SET_SIM_SUBMIT:
      return { 
        ...state, 
        data: null
      }
    default:
      return state
  }
}
