const SET_SIM_OUTLET = 'SET_SIM_OUTLET';
const SET_SIM_OUTLET_SUCCESS = 'SET_SIM_OUTLET_SUCCESS';
const SET_SIM_OUTLET_FAILED = 'SET_SIM_OUTLET_FAILED';

const SET_SIM_DETAIL_OUTLET = 'SET_SIM_DETAIL_OUTLET';
const SET_SIM_DETAIL_OUTLET_SUCCESS = 'SET_SIM_DETAIL_OUTLET_SUCCESS';
const SET_SIM_DETAIL_OUTLET_FAILED = 'SET_SIM_DETAIL_OUTLET_FAILED';

const UNMOUNT_SET_SIM_OUTLET = 'UNMOUNT_SET_SIM_OUTLET';

const initialState = {
  list: null,
  loading: true,
  detail: null
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_SIM_OUTLET:
      return { 
        ...state, 
        loading: true,
        detail: null
      }

    case SET_SIM_OUTLET_SUCCESS:
      return { 
        ...state, 
        loading: false,
        list: payload.list,
        detail: null
      }
    case SET_SIM_OUTLET_FAILED:
      return { 
        ...state, 
        loading: true,
        list: null,
        detail: null
      }
    case SET_SIM_DETAIL_OUTLET_SUCCESS:
      return { 
        ...state, 
        loading: false,
        detail: payload.detail
      }
    case UNMOUNT_SET_SIM_OUTLET:
      return { 
        ...state, 
        loading: true,
        list: null,
        detail: null
      }
    default:
      return state
  }
}
