const SET_SIM = 'SET_SIM';
const SET_SIM_SUCCESS = 'SET_SIM_SUCCESS';
const SET_SIM_FAILED = 'SET_SIM_FAILED';
const UNMOUNT_SET_SIM = 'UNMOUNT_SET_SIM';

const initialState = {
  data: null,
  loading: true,
  status: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_SIM:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    case SET_SIM_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
        status: payload.status,
      }
    case SET_SIM_FAILED:
      return { 
        ...state, 
        loading: false,
        data: null,
        status: payload.status,
      }
    case UNMOUNT_SET_SIM:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
