const SET_LIST_DATA_MASTER = 'SET_LIST_DATA_MASTER';
const SET_LIST_DATA_MASTER_SUCCESS = 'SET_LIST_DATA_MASTER_SUCCESS';
const SET_LIST_DATA_MASTER_FAILED = 'SET_LIST_DATA_MASTER_FAILED';

const SET_LIST_DATA_MASTER_CITY = 'SET_LIST_DATA_MASTER_CITY';
const SET_LIST_DATA_MASTER_CITY_SUCCESS = 'SET_LIST_DATA_MASTER_CITY_SUCCESS';
const SET_LIST_DATA_MASTER_CITY_FAILED = 'SET_LIST_DATA_MASTER_CITY_FAILED';

const UNMOUNT_LIST_DATA_MASTER = 'UNMOUNT_LIST_DATA_MASTER';

const initialState = {
  loading: false,
  data: null,
  city: null
};

export default function jobReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_LIST_DATA_MASTER:
      return {
        ...state,
        loading: true,
      };
    case SET_LIST_DATA_MASTER_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case SET_LIST_DATA_MASTER_FAILED:
      return {
        ...state,
        loading: false,
        data: null,
      };

    case UNMOUNT_LIST_DATA_MASTER:
      return {
        ...state,
        loading: false,
        data: null,
        city: null
      };
    default:
      return state;
  }
}
