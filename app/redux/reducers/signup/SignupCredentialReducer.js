const SET_SIGNUP_CREDENTIAL_SUCCESS = 'SET_SIGNUP_CREDENTIAL_SUCCESS';
const SET_SIGNUP_CREDENTIAL_FAILED = 'SET_SIGNUP_CREDENTIAL_FAILED';

const initialState = {
  data: null,
};

export default function auth(state = initialState, action = {}) {
  switch (action.type) {
    case SET_SIGNUP_CREDENTIAL_SUCCESS:
      return {
        ...state,
        data: action.payload.data,
      };
    case SET_SIGNUP_CREDENTIAL_FAILED:
      return {
        ...state,
        data: false
      };
    default:
      return state;
  }
}
