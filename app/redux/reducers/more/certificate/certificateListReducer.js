const SET_CERTIFICATE_LIST = 'SET_CERTIFICATE_LIST';
const SET_CERTIFICATE_LIST_SUCCESS = 'SET_CERTIFICATE_LIST_SUCCESS';
const SET_CERTIFICATE_LIST_FAILED = 'SET_CERTIFICATE_LIST_FAILED';
const UNMOUNT_CERTIFICATE_LIST = 'UNMOUNT_CERTIFICATE_LIST';

const initialState = {
  data: [],
  detail: null,
  loading: true,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CERTIFICATE_LIST:
      return { 
        ...state, 
        loading: true,
      }
    case SET_CERTIFICATE_LIST_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
      }
    case SET_CERTIFICATE_LIST_FAILED:
      return { 
        ...state, 
        loading: false,
        data: [],
      }
    
    case UNMOUNT_CERTIFICATE_LIST:
      return { 
        ...state, 
        loading: true,
        data: [],
      }
    default:
      return state
  }
}
