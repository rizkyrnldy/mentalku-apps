const SET_CERTIFICATE_DETAIL = 'SET_CERTIFICATE_DETAIL';
const SET_CERTIFICATE_DETAIL_SUCCESS = 'SET_CERTIFICATE_DETAIL_SUCCESS';
const SET_CERTIFICATE_DETAIL_FAILED = 'SET_CERTIFICATE_DETAIL_FAILED';
const UNMOUNT_CERTIFICATE_DETAIL = 'UNMOUNT_CERTIFICATE_DETAIL';

const initialState = {
  data: null,
  loading: true,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_CERTIFICATE_DETAIL:
      return { 
        ...state, 
        loading: true,
      }
    case SET_CERTIFICATE_DETAIL_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
      }
    case SET_CERTIFICATE_DETAIL_FAILED:
      return { 
        ...state, 
        loading: false,
        data: null,
      }
    case UNMOUNT_CERTIFICATE_DETAIL:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
