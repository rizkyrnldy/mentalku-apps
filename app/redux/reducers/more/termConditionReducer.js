const SET_TERMS = 'SET_TERMS';
const SET_TERMS_SUCCESS = 'SET_TERMS_SUCCESS';
const SET_TERMS_FAILED = 'SET_TERMS_FAILED';
const UNMOUNT_SET_TERMS = 'UNMOUNT_SET_TERMS';

const initialState = {
  data: null,
  loading: true,
  status: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_TERMS:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    case SET_TERMS_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
        status: payload.status,
      }
    case SET_TERMS_FAILED:
      return { 
        ...state, 
        loading: false,
        data: null,
        status: payload.status,
      }
    case UNMOUNT_SET_TERMS:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
