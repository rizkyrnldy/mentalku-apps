const SET_PROFILE = 'SET_PROFILE';
const SET_PROFILE_SUCCESS = 'SET_PROFILE_SUCCESS';
const SET_PROFILE_FAILED = 'SET_PROFILE_FAILED';
const UNMOUNT_PROFILE = 'UNMOUNT_PROFILE';

const initialState = {
  loading: true,
  data: null,
};

export default function auth(state = initialState, action = {}) {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        loading: true
      };
    case SET_PROFILE_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case SET_PROFILE_FAILED:
      return {
        ...state,
        loading: true,
        data: null,
      };
    case UNMOUNT_PROFILE:
      return {
        ...state,
        loading: true,
        data: null,
      };
    default:
      return state;
  }
}
