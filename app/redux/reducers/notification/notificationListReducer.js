const SET_LIST_NOTIFICATION = 'SET_LIST_NOTIFICATION';
const SET_LIST_NOTIFICATION_SUCCESS = 'SET_LIST_NOTIFICATION_SUCCESS';
const SET_LIST_NOTIFICATION_FAILED = 'SET_LIST_NOTIFICATION_FAILED';
const UNMOUNT_LIST_NOTIFICATION = 'UNMOUNT_LIST_NOTIFICATION';

const initialState = {
  loading: true,
  data: [],
};

export default function (state = initialState, action = {}) {
  switch (action.type) {
    case SET_LIST_NOTIFICATION:
      return {
        ...state,
        loading: true
      };
    case SET_LIST_NOTIFICATION_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.payload.data,
      };
    case SET_LIST_NOTIFICATION_FAILED:
      return {
        ...state,
        loading: true,
        data: [],
      };
    case UNMOUNT_LIST_NOTIFICATION:
      return {
        ...state,
        loading: true,
        data: [],
      };
    default:
      return state;
  }
}
