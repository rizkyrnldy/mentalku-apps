const SET_DETAIL_PSIKOLOG = 'SET_DETAIL_PSIKOLOG';
const SET_DETAIL_PSIKOLOG_SUCCESS = 'SET_DETAIL_PSIKOLOG_SUCCESS';
const SET_DETAIL_PSIKOLOG_FAILED = 'SET_DETAIL_PSIKOLOG_FAILED';
const UNMOUNT_SET_DETAIL_PSIKOLOG = 'UNMOUNT_SET_DETAIL_PSIKOLOG';

const initialState = {
  data: null,
  loading: true,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_DETAIL_PSIKOLOG:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    case SET_DETAIL_PSIKOLOG_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
      }
    case SET_DETAIL_PSIKOLOG_FAILED:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    case UNMOUNT_SET_DETAIL_PSIKOLOG:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
