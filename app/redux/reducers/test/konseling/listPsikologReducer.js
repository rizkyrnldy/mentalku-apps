const SET_LIST_PSIKOLOG = 'SET_LIST_PSIKOLOG';
const SET_LIST_PSIKOLOG_SUCCESS = 'SET_LIST_PSIKOLOG_SUCCESS';
const SET_LIST_PSIKOLOG_FAILED = 'SET_LIST_PSIKOLOG_FAILED';
const UNMOUNT_LIST_PSIKOLOG = 'UNMOUNT_SET_LIST_PSIKOLOG';

const initialState = {
  data: null,
  loading: true,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_LIST_PSIKOLOG:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    case SET_LIST_PSIKOLOG_SUCCESS:
      return { 
        ...state, 
        loading: false,
        data: payload.data,
      }
    case SET_LIST_PSIKOLOG_FAILED:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    case UNMOUNT_LIST_PSIKOLOG:
      return { 
        ...state, 
        loading: true,
        data: null,
      }
    default:
      return state
  }
}
