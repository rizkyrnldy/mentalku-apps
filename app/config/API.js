import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import { ENV } from './index';
import { Base64 } from 'js-base64';
import { Platform } from 'react-native';
// const FormData = require('form-data');

export const POST = async (URL, payload, progress) => {
  const getToken = await AsyncStorage.getItem('tokenUser');
  console.log('payload post', payload,  `${ENV.HTTP_URL}${URL}`)
  return new Promise((resolve, reject) => {
    return axios.post(`${ENV.HTTP_URL}${URL}`, payload, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${getToken}`,
      },
      onUploadProgress: (progressEvent) => {
        var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        console.log(999, percentCompleted, 'progressEvent');
        return progress && progress(percentCompleted)
      },
    }).then((res) => {
      if(res.data.status.code === 200){
        return resolve(res.data)
      }else{
        const err = res.data.status;
        return reject(err)
      }
    }).catch((err) => {
      // console.log(555, err.response);
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })
}

export const GET = async (URL, payload) => {
  const getToken = await AsyncStorage.getItem('tokenUser');
  console.log('payload get', payload , `${ENV.HTTP_URL}${URL}`)
  return new Promise((resolve, reject) => {
    axios.get(`${ENV.HTTP_URL}${URL}`, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
        "Authorization": `Bearer ${getToken}`,
        "data": payload ? Base64.encode(JSON.stringify(payload)) : ''
      }
    }).then((res) => {
      if(res.data.status.code === 200){
        return resolve(res.data)
      }else{
        const err = res.data.status
        return reject(err)
      }
    }).catch((err) => {
      console.log(222, err.response);
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })   
}


export const WP = async (URL, payload) => {
  console.log('payload get', payload , `${ENV.HTTP_URL_NEWS}${URL}`)
  return new Promise((resolve, reject) => {
    axios.get(`${ENV.HTTP_URL_NEWS}${URL}`, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      }
    }).then((res) => {
      if(res.status === 200){
        return resolve(res)
      }else{
        const err = { status: 400, message: 'FAILED' }
        return reject(err)
      }
    }).catch((err) => {
      console.log(222, err.response);
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })   
}


export const FACE = async (URL, payload, progress) => {
  // console.log('payload post', payload,  `${ENV.HTTP_URL}${URL}`)
  return new Promise((resolve, reject) => {
    return axios.post(`${ENV.HTTP_URL_FACE}${URL}`, payload, {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "application/json",
      },
      onUploadProgress: (progressEvent) => {
        var percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        console.log(123, percentCompleted, 'progressEvent');
        return progress && progress(percentCompleted)
      },
    }).then((res) => {
      // console.log(1234, res.data);
      if(res.data.status.code === 200){
        return resolve(res.data)
      }else{
        const err = res.data.status;
        return reject(err)
      }
    }).catch((err) => {
      console.log(5551, err.response);
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })
}

export const UPLOAD = async (URL, payload) => {
  const getToken = await AsyncStorage.getItem('tokenUser');
  return new Promise((resolve, reject) => {
    var data = new FormData();
    for (const [val, name] of Object.entries(payload)) {
      // console.log(887, val);
      if(val === 'avatar' || val === 'ktp'){
        let fileExtension = name.substr(name.lastIndexOf('.') + 1);
        data.append(val, {
          uri: Platform.OS === 'android' ? name : name.replace('file://', ''),
          name: `avatar_${Math.random(4000)}.${fileExtension}`,
          type: `image/${fileExtension}`,
        });
      }else{
        data.append(val, name);
      }
    } 
    // console.log(222, data);
    axios.post(`${ENV.HTTP_URL}${URL}`, data, {
      headers: {
        "Accept": 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "multipart/form-data",
        "Authorization": `Bearer ${getToken}`,
      },
    }).then((res) => {
      if(res.data.status.code === 200){

        return resolve(res.data)
      }else{
        const err = res.data.status
        return reject(err)
      }
    }).catch((err) => {
      const error = err.response ? err.response.data.status : JSON.parse(JSON.stringify(err));
      return reject(error)
    })
  })   
}