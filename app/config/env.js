const ENV = {
  PATH_ENV : '/',
  // HTTP_URL : 'http://192.168.0.21:3333/api/v1',
  // HTTP_URL : 'https://api-sit.mentalku.com/api/v1',
  HTTP_URL : 'https://api.mentalku.com/api/v1',
  HTTP_URL_FACE : 'http://128.199.88.211:5050',
  HTTP_URL_NEWS : 'https://mentalku.com/wp-json/wp/v2',
}

export default ENV;
