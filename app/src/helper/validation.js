// export const validationForm = (data, scb, fcb) => {
//   var result = {};
//   var err = 0
//   for (const [name, val] of Object.entries(data)) {
//     var _name = name;
//     var _value = val.value;
//     if(_value === ""){
//       result[_name] = {value: '', error: true}
//       err = 1
//     }else{
//       result[_name] = {value: _value, error: false}
//       err = 0;
//     }
//   }
//   return err === 0 ? scb(result) : fcb(result);
// }

// export const validationForm = (data, scb, fcb) => {
//   var array = [];
//   var error = 0;
//   var result = {};
//   console.log(1234, data);
//   data.forEach(res => {
//     var _res = res;
//     if(!res.value || (res.type === 'autocomplete' && !res.value.value)){
//       console.log(12345, 'err');
//         _res.error = true;
//         error = 1;
//         return array.push(_res);
//     }else{
//       console.log(12345, 'g err');
//       _res.error = false;
//       error = 0;
//       result[res.name] = res.type === 'autocomplete' ? res.value.value : res.value
//       return array.push(_res);
//     }
//   });
//   return error === 0 ? scb(result) : fcb(array);
// }

export const validationForm = (data, scb, fcb) => {
  var getError = data.filter(e => {
    return  !e.value || (e.type === 'autocomplete' && !e.value.value);
  });
  if(getError.length > 0){
    var _data = getError.map((res) => {
      var nData = res;
      res.error = true
      return nData;
    });
    var compareData = arrayUnique(data.concat(_data));
    return fcb(compareData);
  }else{
    var nData = {};
    data.forEach((res) => {
      return nData[res.name] = res.type === 'autocomplete' ? res.value.value : res.value;
    });
    return scb(nData) 
  }
}

const arrayUnique = (array) => {
  var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
}

