export function stringUppercase(str){
  return str.toLowerCase()
  .replace(/(^|[^a-z0-9-])([a-z])/g,
    function(m, m1, m2, p) {
      return m1 + m2.toUpperCase();
    });
}

export function camelCaseToCapital(str){
  return str && str !== undefined && str !== null ? str.split(/(?=[A-Z])/).join(' ') : 'null'
}

export function capitalToCamelCase(str){
  return str
        .replace(/\s(.)/g, function($1) { return $1.toUpperCase(); })
        .replace(/\s/g, '')
        .replace(/^(.)/, function($1) { return $1.toLowerCase(); });
}

export function stringCapital(str){
  return str.toUpperCase()
}

export function stringLowercase(str){
  return str.toLocaleLowerCase()
}

export function numberZeroUnderTen(num){
  return (num < 10) ? '0' + num.toString() : num.toString();
}

export const capitalize = (str) => {
  return str.charAt(0).toUpperCase() + str.slice(1);
}