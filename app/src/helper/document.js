import { Base64 } from 'js-base64';

export const download = (type, caseId, action, data, successCB, failedCB) => {
  return action(type, caseId, data, (res) => {
    const resData = res.body;
    const blob = b64toBlob(resData.content, resData.type);
    const blobUrl = URL.createObjectURL(blob);
    var downloadLink = document.createElement("a");
    downloadLink.href = blobUrl;
    downloadLink.download = resData.fileName;  
    return ( successCB(resData), downloadLink.click() )
  }, (err) => {
    return failedCB(err)
  })
}

export const upload = (file, successCB, failedCB) =>{
  return getBase64(file, (result) => {
    const bodyData = { 
      size: file.size,
      name: file.name, 
      type: file.type, 
      content: result 
    }; 

    return successCB(bodyData)
    // return action(bodyData, type, caseId, data, (resData) => {
    //   return successCB(resData);
    // }, (err) => {
    //   return failedCB(err)
    // })
  });
}

export const documentView = (file, successCB, failedCB) => {
  // var resData = file;
  // console.log(555, resData.content);
  // var content = resData.content.replace(`data:${resData.type};base64,`, '');
  const blob = b64toBlob(file, 'application/pdf');
  const blobUrl = URL.createObjectURL(blob);
  // resData.content = blobUrl;
  return successCB && successCB(blobUrl)
}

export const uploadChunks = async (file, type, caseId, action, successCB, failedCB) =>{
  return getBase64(file, (result) => {
    const chunksFile = result.split(',')[1];
    const chunkSize = 1024 * 10;
    const chunks = chunksFile.match( new RegExp(".{1," + chunkSize + "}", "g") );
    const data = { name: file.name, type: file.type, content: chunks};
    return action(data, type, caseId, (resData) => {
      return successCB(resData);
    }, (err) => {
      return failedCB(err)
    })
  });
}

export const b64toBlob = (b64Data, contentType='', sliceSize=512) => {
  const byteCharacters = Base64.atob(b64Data);
  const byteArrays = [];
  for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    const slice = byteCharacters.slice(offset, offset + sliceSize);
    const byteNumbers = new Array(slice.length);
    for (let i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
    const byteArray = new Uint8Array(byteNumbers);
    byteArrays.push(byteArray);
  }

  const blob = new Blob(byteArrays, {type: contentType});
  return blob;
}

const getBase64 = (file, cb) => {
  let reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = function () {
    cb(reader.result);
  };
  reader.onerror = function (error) {
    console.log('Error: ', error);
  };
}