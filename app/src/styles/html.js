import { StyleSheet } from 'react-native';

const html = StyleSheet.create({

  defaultStyle: {
    // color: 'red',
    fontFamily: 'FiraSans',
    lineHeight: 25
  },

  
  h1: {
    fontSize: 22,
    fontFamily: 'FiraSans_bold',
    lineHeight: 25
  },

  h2: {
    fontSize: 20,
    fontFamily: 'FiraSans_bold',
    lineHeight: 25
  },

  h3: {
    fontSize: 18,
    fontFamily: 'FiraSans_bold',
    lineHeight: 25
  },
  
  b: {
    fontFamily: 'FiraSans_bold',
  },

  strong: {
    fontFamily: 'FiraSans_bold',
  },
  
  em: {
    fontFamily: 'FiraSans_italic',
  },


  a: {
    color: '#15aa1a'
  },

  p: {
    // fontFamily: 'FiraSans',
    marginBottom: -15,
    // lineHeight: 22
  },

  i: {
    fontFamily: 'FiraSans_italic'
  },

  ol: {
    marginLeft: 10,
  },
  
  ul: {
    marginLeft: 10,
  },
  
  li: {
    lineHeight: 35,
  }

});


module.exports = html;
