import styles from './styles';
import global from './global';
import html from './html';

export {
    styles,
    global,
    html
}