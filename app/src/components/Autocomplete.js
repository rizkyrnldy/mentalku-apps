import React, { Component } from 'react'
import { View, ActivityIndicator, Platform, Dimensions } from 'react-native'
import { connect } from 'react-redux'
import { Container, Content, Icon, Right, Input, Item,  Left, Label, Text, List, ListItem, Spinner, Header, Button } from 'native-base';
import debounce from 'lodash.debounce';
import { global } from '../styles'
import { listDataMasterDomicile, unmoutListDataMaster } from '../../redux/actions/component/dataMasterAction'
import Constants from 'expo-constants';

class Autocomplete extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _value: this.props.value
    }

    this.onSearch = debounce(this.onSearch.bind(this), 500);
  }

  onChangeText = (e) => {
    this.setState({ _value: e }, () => this.onSearch(e))
  }

  onSearch = (e) => {
    const { actionlistDataMasterDomicile } = this.props;
    return actionlistDataMasterDomicile(e)
  }
  
  onPress(e){
    const { onChange, actionunmoutListDataMaster } = this.props;
    this.setState({ _value: e.label, }, () => onChange(e))
    return actionunmoutListDataMaster()
  }

  render(){
    const { label, getDataMaster: { data,loading }, error, onClose } = this.props
    const { _value } = this.state
    return(
      <Container style={{position: 'relative', flex: 1}}>
        <Header searchBar rounded style={{paddingLeft: 60}}>
          <Item error={error}>
            <Icon name="ios-search" />
            <Input 
              placeholder={`Cari ${label}`}
              onChangeText={this.onChangeText}
            />
            {loading && <ActivityIndicator style={[{ marginRight: 10 }]} size="small" color="#BE1E2D" />}
          </Item>
        </Header>
        <Button transparent onPress={() => onClose()} style={{position: 'absolute', top: Platform.OS === 'android' ? 4 : Constants.statusBarHeight + 8, left: 0}}>
          <Icon type="Ionicons" name="arrow-back" style={{color: '#000'}} />
        </Button>
        
        <Content style={{ backgroundColor: '#fff' }}>
          {loading ? <Spinner color='#15aa1a' size="small" style={{height: 10 }}/> : data ? data.map((res, i) => {
            return(
              <List key={i}>
                <ListItem onPress={() => this.onPress(res)} button>
                  <Left>
                    <Text style={{fontSize: 16, textAlign: 'left'}}>{res.label}</Text>
                  </Left>
                  <Right>
                    <Icon type="AntDesign" name="right" />
                  </Right>
                </ListItem>
              </List>
            )
          }) : null}
        </Content>
      </Container>
    )
  }


  renderss() {
    const { label, getDataMaster: { data,loading }, error} = this.props
    const { _value } = this.state
    return (
      <View>
        <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>{label}</Label>
        <Item regular error={error}>
          <Input 
            placeholder="Domisili" 
            returnKeyType="next" 
            placeholderTextColor="#000"
            value={_value}
            style={{color: '#000'}}
            onChangeText={this.onChangeText}
          />
          { loading && <Spinner color='#15aa1a' size="small" style={{height: 10 }}/> }
        </Item>
        <View style={[global.shadow, {position: 'absolute', top: 85, right: 0, left: 0, backgroundColor: '#fff', zIndex: 2}]}>
          <Content style={{maxHeight: 200}}>
            {data ? data.map((res, i) => {
              return(
                <List key={i}>
                  <ListItem onPress={() => this.onPress(res)} button>
                    <Left>
                      <Text note style={{fontSize: 13}}>{res.label}</Text>
                    </Left>
                  </ListItem>
                </List>
              )
            }) : null}
          </Content>
        </View>
      </View>
    )
  }

}


const mapStateToProps = (state) => ({
  getDataMaster: state.component.dataMaster,
})

const mapDispatchToProps = {
  actionlistDataMasterDomicile: listDataMasterDomicile,
  actionunmoutListDataMaster: unmoutListDataMaster
}

export default connect(mapStateToProps, mapDispatchToProps)(Autocomplete)
