import React, { Component } from 'react';
import { View, Alert, TextInput, Picker } from 'react-native';
import { global } from '../styles';

export default class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: null,
      listCity: ['Jakarta', "Bekasi", "Depok", "Tanggerang"],
      store: ''
    };
  }
  actionSearch(){
    Alert.alert('Cari Tenant')
  }

  render() {
    const {listCity} = this.state;
    return (
      <View style={[{flexDirection: 'row', marginBottom: 10}]}>
        <View style={{flexDirection: 'row', flex: 0, marginRight: 10,  backgroundColor: 'yellow', borderRadius: 5}}>
          <Picker
            selectedValue={this.state.city}
            style={{height: 40, width: 120,}}
            onValueChange={(itemValue, itemIndex) =>{
              this.setState({city: itemValue}) 
            }}
          >
            {listCity.map((res, i) => <Picker.Item key={i} label={res} value={res} />)}
          </Picker>
        </View>

        <View style={{flexDirection: 'row', flex: 1}}>
          <TextInput
            ref="store"
            placeholder="Cari Tenant"
            placeholderTextColor="#cdcdcd"
            returnKeyType='done'
            style={[global.formSearch]}
            underlineColorAndroid='transparent'
            value={this.state.store}
            onChangeText={(val) => this.setState({ store: val })}
            onSubmitEditing={() => this.actionSearch()}
          />
        </View>
      </View>
    );
  }
}
