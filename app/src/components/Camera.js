import React, { Component } from 'react'
import { View, Dimensions, Vibration, TouchableOpacity, Image } from 'react-native';
import { Container, Text, Header, Icon, Left, Body, Right, Title, Button, } from 'native-base';
import { Camera } from 'expo-camera';
import { global } from '../styles';
import * as ImageManipulator from "expo-image-manipulator";

export default class NewCamera extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _flash: false,
      _btnLoading: false,
      _image: null
    }
  }

  async takePicture(){
    const { onSubmit } = this.props;
    const { _image } = this.state
    // Vibration.vibrate();
    if(_image){
      const resizedPhoto = await ImageManipulator.manipulateAsync(
        _image,
        [{ resize: { width: 600, height: 600 } }],
        { compress: 1, format: "jpeg", base64: true }
      );
      return onSubmit && onSubmit(resizedPhoto.uri);
    }else{
      const photo = await this.camera.takePictureAsync({ skipProcessing: true });
      return this.setState({
        _image: photo.uri
      })
    }
  }

  closeImage(){
    const { onCloseModal } = this.props
    const { _image } = this.state
    if(_image){
      return this.setState({ _image: null, _btnLoading: false })
    }else{
      return onCloseModal()
    }
  }

  async flashRotate(){
    const { _image } = this.state
    if(_image){
      const resizedPhoto = await ImageManipulator.manipulateAsync(
        _image,
        [{ rotate: -90 }],
      );
      return this.setState({ _image: resizedPhoto.uri, _btnLoading: false })
    }else{
      return this.setState({_flash: !this.state._flash})
    }
  }

  render() {
    const { _flash, _btnLoading, _image } = this.state
    const { width } = Dimensions.get('window');
    return (
      <Container>
        <View style={{flex: 1, backgroundColor: '#000'}}>
          <View style={{ flex: 1, justifyContent: 'center' }}>
            { _image ? 
              <Image source={{uri: _image}} style={{width: width, height: width}} />
              :
              <Camera 
                ref={ref => { this.camera = ref }}
                // style={{ width: width, height: width }} 
                style={{width: width, height: width}}
                flashMode={_flash ? Camera.Constants.FlashMode.on : Camera.Constants.FlashMode.off}
                type={Camera.Constants.Type.back}
                ratio={'1:1'}
                autoFocus={Camera.Constants.AutoFocus.on}
              />
            }
          </View>
          
          <View style={[global.justify, {backgroundColor: '#000', position: 'absolute', left: 0, right: 0, bottom: 0, padding: 20, paddingBottom: Platform.OS === 'android' ? 20 : 40, alignItems: 'center'}]}>
            <TouchableOpacity onPress={() => this.closeImage()} style={{ flexDirection: 'row', alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderWidth: 1, width: 40, height: 40, borderRadius: 100, overflow: 'hidden', borderColor: '#fff'}}>
              <Icon name="close" type="AntDesign" style={{fontSize: 20, color: '#fff'}}/>
            </TouchableOpacity>

            <TouchableOpacity disabled={_btnLoading} onPress={() => this.takePicture()} style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center', borderWidth: 1, width: 80, height: 80, borderRadius: 100, overflow: 'hidden', borderColor: '#fff', marginHorizontal: 50}}>
              {_image ?
                <Icon name="checkcircle" type="AntDesign" style={{fontSize: 70, color: '#fff'}}/>
                : <View style={{backgroundColor: '#fff', width: 70, height: 70, borderRadius: 100, }} />
              }
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.flashRotate()} style={{ backgroundColor: _flash ? '#fff' : '#000', flexDirection: 'row', alignSelf: 'center', alignItems: 'center', justifyContent: 'center', borderWidth: 1, width: 40, height: 40, borderRadius: 100, overflow: 'hidden', borderColor: '#fff'}}>
              <Icon name={_image ? "rotate-ccw" : "flash"} type={_image ? "Feather" : "Entypo"} style={{fontSize: 20,  color: _flash ? '#000' : '#fff'}}/>
            </TouchableOpacity>
          </View>
        </View>
      </Container>
    )
  }

}