import React, { Component } from 'react'
import { View, Platform } from 'react-native';
import { connect } from 'react-redux'
import { Header, Body, Title, Text, Icon, Right } from 'native-base';
import Constants from 'expo-constants';
import { global } from '../styles';

class Headers extends Component {
  render() {
    const { title, show, getAuth: { dataUser } } = this.props;
    return (
      <View style={{flexDirection: 'column',}}>
        <Header rounded style={{marginTop: Platform.OS === 'android' ? Constants.statusBarHeight : 0, height: !show ? 0 : 60}}>
          <Body>
            <Title>{title}</Title>
          </Body>
          {/* <Right>
            <Button transparent>
              <Icon name='ios-notifications' />
            </Button>
          </Right> */}
        </Header>
        {(dataUser.strength.includes('faceRecog') ||  dataUser.strength.includes('contacts'))&&
          <View style={[global.centered, {flex: 0, backgroundColor: '#ff0000', paddingTop: 10, paddingBottom: 10}]}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name="ios-alert" style={{marginRight: 5, color: '#fff', fontSize: 13}}/> 
              <Text style={{color: '#fff', textAlign: 'center', fontSize: 14}}>Lengkapi biodata anda sekarang</Text>
            </View>
          </View>
        }
      </View>
    )
  }
}



const mapStateToProps = (state) => ({
  getAuth: state.auth
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, mapDispatchToProps)(Headers)
