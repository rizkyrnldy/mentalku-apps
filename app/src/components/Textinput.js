import React, { Component } from 'react'
import { ToastAndroid, View, TouchableOpacity, Modal } from 'react-native';
import { Autocomplete } from './index';
import { Input, Item, Label, Picker, Icon, Text } from 'native-base';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import { global } from '../styles';

export default class Textinput extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _modalDatePicker: -1,
      _modalAutoComplete: false
    }
  }

  render() {
    const { data } = this.props;
    return(
      data.map((res, i) => {
        switch (res.type) {
          case 'autocomplete':
            return this.renderAutoComplete(res, i);
          case 'picker':
            return this.renderPicker(res, i);
          case 'datePicker':
            return this.renderDatePicker(res, i);
          default:
            return this.renderInput(res, i);
        }
      })
    )
  }

  renderInput(res, i){
    const { onChangeText } = this.props;
    return(
      <View style={{marginBottom: 20, position: 'relative', zIndex: -i}} key={i}>
        <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>{res.title}:</Label>
        <Item error={res.error} regular>
          <Input 
            // ref={(c) => this._fullName_ = c}
            returnKeyType="next" 
            placeholderTextColor="#ddd"
            value={res.value}
            placeholder={res.title}
            keyboardType={res.keyboardType ? res.keyboardType : 'default'}
            autoCapitalize={res.capital ? res.capital : 'none'}
            multiline={res.type === 'textArea' ? true : false}
            numberOfLines={res.type === 'textArea' ? 6 : 1}
            textAlignVertical={res.type === 'textArea' ? "top" : "center"}
            style={{paddingTop: res.type === 'textArea' ? 10 : 0, height: res.type === 'textArea' ? 100 : 45, color: '#000'}}
            onChangeText={(e) => onChangeText(e, i)}
            // onSubmitEditing={() => this._gender_._root.focus() }
          />
        </Item>
      </View>
    )
  }

  renderDatePicker(res, i){
    const { onChangeText } = this.props;
    const { _modalDatePicker } = this.state;
    const subtract = moment().subtract(17, 'years').toISOString();
    const maxDate = new Date(subtract);
    return(
      <View style={{marginBottom: 20,  position: 'relative', zIndex: -i}} key={i}>
        <Text bold style={{fontSize: 13, color: '#000', marginBottom: 5, marginTop: 8}}>
          {res.title}:
        </Text>
        <TouchableOpacity style={[global.justify, { borderColor: '#ddd', borderWidth: 1, padding: 10, paddingVertical: 15, marginTop: 5 }]} onPress={() => this.setState({_modalDatePicker: this.state._modalDatePicker === -1 ? i : -1 })}>
          <Text bold={false} style={{ fontSize: 15, color: res.value ? '#000' : '#ddd'}}>
            {
              res.value ? 
                moment(new Date(res.value)).format('DD MMMM YYYY') 
              :`Pilih ${res.title}`
            } 
          </Text>
          <Icon type="AntDesign" name={_modalDatePicker != -1 ? "caretup" : "caretdown"} style={{fontSize: 12, alignSelf: 'center' }}/>
        </TouchableOpacity>
        
        {_modalDatePicker === i &&
          <DateTimePicker
            testID="dateTimePicker"
            maximumDate={maxDate}
            value={res.value ? new Date(res.value) : new Date()}
            mode={res.mode ? res.mode : 'date'}
            is24Hour={true}
            // display="default"
            display="spinner"
            onChange={(e, selectedDate) => {
              return this.setState({_modalDatePicker: -1}, () => {
                return onChangeText(moment(selectedDate).format('YYYY-MM-DD'), i)
              })
            }}
          />
        }
      </View>
    )
  }

  renderAutoComplete(res, i){
    const { _modalAutoComplete } = this.state;
    const { onChangeText } = this.props;
    return(
      <View style={{ marginBottom: 20}} key={i}>
        <Text bold style={{fontSize: 13, color: '#000', marginBottom: 5, marginTop: 8}}>
          {res.title}:
        </Text>
        <TouchableOpacity style={[global.justify, { borderColor: res.error ? '#ed2f2f' : '#ddd', borderWidth: 1, padding: 10, paddingVertical: 15, marginTop: 5  }]} onPress={() => this.setState({ _modalAutoComplete: true })}>
          <Text bold={false} style={{fontSize: 15, color: res.value.label ? '#000' : '#ddd'}}>
            {res.value.label ? res.value.label : `Pilih ${res.title}`} 
          </Text>
          <Icon type="AntDesign" name="caretdown" style={{fontSize: 12, alignSelf: 'center'}}/>
        </TouchableOpacity>
        <Modal
          animationType="slide"
          transparent={false}
          visible={_modalAutoComplete}>
            <Autocomplete 
              label={res.title}
              value={res.value.label}
              onChange={(e) => {
                return this.setState({ _modalAutoComplete: false }, () => onChangeText(e, i))
              }}
              onClose={(e) => this.setState({ _modalAutoComplete: false }) }
              error={res.error}
            />
        </Modal>
      </View>
    )
  }

  renderPicker(res, i){
    const { onChangeText } = this.props;
    return(
      <View style={{ marginBottom: 20,  position: 'relative', zIndex: -i }} key={i}>
        <Text bold style={{ fontSize: 14, color: '#000', marginBottom: 5, marginTop: 8 }}>
          {res.title}:
        </Text>
        <View style={{ height: 50, borderColor: res.error ? '#ed2f2f' : '#ddd', borderWidth: 1}}>
          <Picker
            style={{ width: '100%',}}
            textStyle={{ color: '#000', fontSize: 15 }}
            mode="dropdown"
            iosIcon={<Icon type="AntDesign" name="caretdown" style={{fontSize: 12, marginTop: 5, marginRight: 0, color: '#000', paddingBottom: 15 }}/>}
            placeholderIconColor="red"

            iosHeader={`${res.title}`}
            headerBackButtonTextStyle={{ color: "#000" }}
            headerTitleStyle={{ color: "#000" }}
            
            placeholder={res.title}
            placeholderStyle={{ color: "#000" }}

            selectedValue={res.value}
            onValueChange={(e) => onChangeText(e, i)}
          >
            <Picker.Item label="Pilih Jenis kelamin" value="" />
            <Picker.Item label="Laki-Laki" value="Male" />
            <Picker.Item label="Perempuan" value="Female" />
          </Picker>
        </View>
      </View>
    )
  }

  renderPickers(res, i){
    const { onChangeText } = this.props;
    return(
      <View style={{marginBottom: 20}} key={i}>
        <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>{res.title}:</Label>
          <Item error={res.error} regular>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{ width: '100%' }}
              placeholder="Jenis Kelamin"
              placeholderStyle={{ color: "#bfc6ea" }}
              placeholderIconColor="#007aff"
              selectedValue={res.value}
              onValueChange={(e) => onChangeText(e, i)}
            >
              <Picker.Item label={res.title}/>
              <Picker.Item label="Laki-Laki" value="Male" />
              <Picker.Item label="Perempuan" value="Female" />
            </Picker>
          </Item>
      </View>
    )
  }

  
}
