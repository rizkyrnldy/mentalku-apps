import React, { Component } from 'react'
import { Alert } from 'react-native';
import { Container, Content, Separator, List, ListItem, Text, Icon, Left, Right } from 'native-base';
import { Header } from '../../components';
import { connect } from 'react-redux'
// import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { setLogout } from '../../../redux/actions/authAction';
import Constants from 'expo-constants';

export class index extends Component {
  render() {
    const { navigation } = this.props;
    return (
      <Container>
        <Header show={true} title="Lainnya"/>
        <Content style={{backgroundColor: '#fff'}}>
          <List>
            <ListItem button onPress={() => navigation.navigate('CertificateListPage')}>
              <Left>
                <Text>Sertifikat</Text>
              </Left>
              <Right>
                <Icon name="ios-arrow-forward" />
              </Right>
            </ListItem>
          </List>
          
          <List>
            <ListItem button onPress={() => navigation.navigate('TermConditionPage')}>
              <Left>
                <Text>Syarat & Ketentuan</Text>
              </Left>
              <Right>
                <Icon name="ios-arrow-forward" />
              </Right>
            </ListItem>
          </List>
          
          <List>
            <ListItem button onPress={() => navigation.navigate('SupportPage')} last>
              <Left>
                <Text>Pusat Bantuan</Text>
              </Left>
              <Right>
                <Icon name="ios-arrow-forward" />
              </Right>
            </ListItem>
          </List>
          
          <Separator bordered />
          
          <List>
            <ListItem button onPress={() => this.onLogout()} last>
              <Left>
                <Text>Keluar</Text>
              </Left>
              <Right>
                <Icon name="ios-arrow-forward" />
              </Right>
            </ListItem>
          </List>
          <Text style={{textAlign: 'center', marginTop: 20, fontSize: 12}}>
            Mentalku V.{Constants.manifest.version}
          </Text>
        </Content>
      </Container>
    )
  }

  onLogout(){
    return Alert.alert( "Apakah anda yakin keluar?", "",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.onLogoutFunction() }
      ],
      { cancelable: false }
    );
  }

  onLogoutFunction(){
    const { actionReqLogout } = this.props;
    return actionReqLogout(() => AsyncStorage.removeItem('@loginUser:key'), () => AsyncStorage.removeItem('@loginUser:key'))
  }

}

const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionReqLogout: setLogout 
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
