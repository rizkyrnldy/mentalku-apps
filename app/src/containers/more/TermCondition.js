import React, { Component } from 'react'
import { Container, Content } from 'native-base';
import { connect } from 'react-redux'
import { global, html } from '../../styles'
import { setTerms, unmountTerms } from '../../../redux/actions/more/termsAction';
import { Loading } from '../../components';
import HTMLView from 'react-native-htmlview';

export class TermCondition extends Component {
  componentDidMount() {
    const { actionsetTerms } = this.props;
    return actionsetTerms()
  }
    
  render() {
    const { getData: { data, loading } } = this.props;
    if(loading){
      return <Loading />
    }
    return (
      <Container style={[global.bgWhite]}>
        <Content padder>
          <HTMLView 
            stylesheet={html}
            value={data.content} 
          />
        </Content>
      </Container>
    )
  }

  componentWillUnmount(){
    const { actionunmountTerms } = this.props;
    return actionunmountTerms();
  }
}


const mapStateToProps = (state) => ({
  getData: state.more.termCondition,
})

const mapDispatchToProps = {
  actionsetTerms: setTerms,
  actionunmountTerms: unmountTerms
}

export default connect(mapStateToProps, mapDispatchToProps)(TermCondition)
