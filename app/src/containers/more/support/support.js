import React, { Component } from 'react'
import { View } from 'react-native'
import { connect } from 'react-redux'
import { validationForm } from '../../../helper';
import { NewContainer, Textinput } from '../../../components'
import { Content, Form, Button, Text, Toast } from 'native-base';
import { setSupport } from '../../../../redux/actions/more/supportAction'
import update from 'react-addons-update';
import { global } from '../../../styles';

export class Support extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _loading: false,
       _form: [
        {name: 'title', title: 'Judul', value: '', error: false, capital: "words", required: true},
        {name: 'description', title: 'Keterangan', value: '', type: 'textArea', error: false,  required: true},
       ],
    }
  }

  render() {
    const { _loading, _form } = this.state;
    return (
      <NewContainer loading={_loading}>
        <Content padder>
        <Form>
           <Textinput 
              data={_form} 
              onChangeText={(e, i) => this.setState({
                _form: update(this.state._form, { 
                  [i]: { value: { $set: e }  }
                }) 
              })}
            />
        </Form>
        </Content>
        <View style={[global.container]}>
          <Button block rounded  onPress={() => this.onSubmit()} loading={true}>
            <Text style={{fontSize: 16}}>
              KIRIM
            </Text>
          </Button>
        </View>
      </NewContainer>
    )
  }

  onSubmit(){
    const { _form  } = this.state;
    const { actionsetSupport, navigation } = this.props;
    return this.setState({_loading: true}, () => {
      return validationForm(_form, (res) => {
        return actionsetSupport(res, (_res) => {
          return this.setState({ _loading: false }, () => {
            Toast.show({ text: _res.status.message, type: "success" }) 
            return navigation.goBack()
          })
        }, () => {
          Toast.show({ text: 'Data Gagal Terkirim', type: "danger" }) 
          return navigation.goBack()
        }) 
      }, (err) => {
        return this.setState({
          _form: err,
          _loading: false
        }, () => Toast.show({ text: "Form Tidak Boleh Kosong", type: "danger" }) )
      })
    })
  }
}

const mapStateToProps = (state) => ({
  getAuth: state.auth,
})

const mapDispatchToProps = {
  actionsetSupport: setSupport
}

export default connect(mapStateToProps, mapDispatchToProps)(Support)
