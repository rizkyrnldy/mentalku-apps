import React, { Component } from 'react'
import { View, RefreshControl, Dimensions } from 'react-native'
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, H3, Spinner } from 'native-base';
import { connect } from 'react-redux'
import { Header, Loading, Empty } from '../../../components'
import { global } from '../../../styles'
import { setListCertificate, setListCertificateMore, unmountListCertificate } from '../../../../redux/actions/more/certificateAction'
import moment from 'moment';
const { height } = Dimensions.get('window')

export class CertificateList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _type: 'created_at',
      _orderBy: 'desc',
      _loading: false
    }
  }

  componentDidMount(){
    return this.getData();
  }

  getData(){
    const { actionsetListCertificate } = this.props;
    const { _current, _type, _orderBy } = this.state;
    var payload = {
      current: _current,
      type: _type,
      orderBy: _orderBy,
    }
    return actionsetListCertificate(payload);
  }
  
  onRefresh(){
    this.setState({ _loading: true, _current: 0 }, () => {
      this.setState({ _loading: false })
      return this.getData();
    })
  }

  setCurrentReadOffset = (event) => {
    const { actionsetListCertificateMore } = this.props;
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    if(scrollHeight == event.nativeEvent.contentSize.height){
      return this.setState({ _current: this.state._current + 1, _loading: true }, () => {
        var value = { 
          current: this.state._current,
          type: this.state._type,
          orderBy: this.state._orderBy
        }
        return actionsetListCertificateMore(value, () => {
          this.setState({_loading: false})
        });
      })
      
    }
  }

  render() {
    const { getData: { data, loading }, navigation } = this.props;
    const { _loading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <Container style={{backgroundColor: '#fff'}}>
        <Content 
          refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />}
          scrollEventThrottle={300}
          onScroll={this.setCurrentReadOffset}
        >
          <List>
            {data.length > 0 ? data.map((res, i) => {
              return(
                <ListItem key={i} onPress={() => navigation.navigate('CertificateDetailPage', { data: res })} button>
                  <Body>
                    <Text bold>{res.title}</Text>
                    <Text note style={{marginBottom: 15, marginTop: 5}}>
                      {res.name} {res.type} {res.category}
                    </Text>
                    <View style={[global.justify]}>
                      <Text note>{res.outletName}</Text>
                      <Text note>{moment(res.updated_at).format('DD MMMM YYYY H:m:s')}</Text>
                    </View>
                  </Body>
                </ListItem>
              )
            }): <Empty text="Tidak ada sertifikat"/>}
          </List>
          {_loading && <Spinner />}
        </Content>
      </Container>
    )
  }

  componentWillUnmount(){
    const { actionunmountListCertificate } = this.props;
    return actionunmountListCertificate()
  }
}

const mapStateToProps = (state) => ({
  getData: state.more.certificate.list
})

const mapDispatchToProps = {
  actionsetListCertificate: setListCertificate,
  actionsetListCertificateMore: setListCertificateMore,
  actionunmountListCertificate: unmountListCertificate
  
}

export default connect(mapStateToProps, mapDispatchToProps)(CertificateList)
