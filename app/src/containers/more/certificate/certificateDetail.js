import React, { Component } from 'react'
import { View, RefreshControl, Dimensions } from 'react-native'
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, H3, Spinner } from 'native-base';
import { connect } from 'react-redux'
import { Header, Loading, Empty } from '../../../components'
import { b64toBlob, documentView } from '../../../helper';
import { global } from '../../../styles'
import { setDetailCertificate, unmountDetailCertificate } from '../../../../redux/actions/more/certificateAction'
import PDFReader from 'rn-pdf-reader-js'


export class CertificateDetail extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount(){
    const { actionsetDetailCertificate, route: { params: { data } } } = this.props;
    console.log(222, data.uid);
    var payload = { uid: data.uid }
    return actionsetDetailCertificate(payload);
  }

  render() {
    const { getData: { data, loading } } = this.props;
    // console.log(222, getData);
    if(loading){
      return <Loading />
    }
    return (
      <Container style={{backgroundColor: '#fff'}}>
          <PDFReader
            withScroll={true}
            noLoader={true}
            withPinchZoom={true}
            source={{
              base64: data,
            }}
            onLoad={(e) => console.log(44, e)}
          />
      </Container>
    )
  }

  componentWillUnmount(){
    const { actionunmountDetailCertificate } = this.props;
    return actionunmountDetailCertificate()
  }
}

const mapStateToProps = (state) => ({
  getData: state.more.certificate.detail
})

const mapDispatchToProps = {
  actionsetDetailCertificate: setDetailCertificate,
  actionunmountDetailCertificate: unmountDetailCertificate  
}

export default connect(mapStateToProps, mapDispatchToProps)(CertificateDetail)
