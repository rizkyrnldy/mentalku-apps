import Landing from './login/Landing';
import Login from './login/Login';
import Verification from './login/Verification';

// Tabbar
import Home from './home';
import Billing from './billing';
import Profile from './profile';
import Notification from './notification';
import More from './more';

import EditProfileFaceRecog from './profile/edit/EditProfileFaceRecog';
import EditProfileInformation from './profile/edit/EditProfileInformation';

import ResultQuiz from './home/quiz/ResultQuiz';

    import Sim from './home/sim';
    // create sim
    import CreateSimSearchOutlet from './home/sim/create/CreateSimSearchOutlet';
    import CreateSimTypePrice from './home/sim/create/CreateSimTypePrice';
    import ConfirmationSim from './home/sim/create/ConfirmationSim';
    
    import Reader from './home/reader/Reader';
    import ReaderDetail from './home/reader/ReaderDetail';
    
    import ListPsikolog from './home/konseling/ListPsikolog';
    import DetailKonseling from './home/konseling/DetailKonseling';
    import ConfirmationKonseling from './home/konseling/ConfirmationKonseling';
    
import ListNews from './home/news/ListNews';
import DetailNews from './home/news/DetailNews';


import TermCondition from './more/TermCondition';
import CertificateList from './more/certificate/certificateList';
import CertificateDetail from './more/certificate/certificateDetail';
import Support from './more/support/support';


export{
    Landing,
    Login,
    Verification,
    
    Home,
    Profile,
    Billing,
    Notification,
    More,

    ResultQuiz,

    EditProfileFaceRecog,
    EditProfileInformation,
    // VerificationCredential,

    Sim,
    ConfirmationSim,
    CreateSimSearchOutlet,
    CreateSimTypePrice,


    Reader,
    ReaderDetail,
    ListPsikolog,
    DetailKonseling,
    ConfirmationKonseling,
    
    ListNews,
    DetailNews,

    TermCondition,
    CertificateList,
    CertificateDetail,
    
    Support
    
    // Face
}