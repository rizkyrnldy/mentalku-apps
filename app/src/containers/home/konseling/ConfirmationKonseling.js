import React, { Component } from 'react'
import { View, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import { Container, Content, Card, CardItem, Radio, Text, Button, Body } from 'native-base';
import { setBookingPsikolog } from '../../../../redux/actions/test/konseling/KonselingTestAction'
import { global } from '../../../styles';
import moment from 'moment';

class ConfirmationKonseling extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false,
    }
  }
  
  render() {
    const { getAuth: { dataUser }, route: { params: { data } } } = this.props;
    const { _btnLoading } = this.state;
    return (
      <Container>
        <Content padder>
          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Data Diri
                </Text>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Nama Lengkap: </Text>
                  <Text note>{dataUser.fullName}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Email: </Text>
                  <Text note>{dataUser.email}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>No. Telepon: </Text>
                  <Text note>{dataUser.phoneNumber}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>NIK: </Text>
                  <Text note>{dataUser.nik}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>

          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Data Dokter Psikologi
                </Text>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Nama Dokter: </Text>
                  <Text note>{data.doctor.fullName}</Text>
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 5, flexWrap: 'wrap'}}>
                  <Text style={{fontWeight: 'bold'}}>Spesialis: </Text>
                  <Text note>{`${data.doctor.doctorContact.specialist}`}</Text>
                </View>

              </Body>
            </CardItem>
          </Card>
          
          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Jadwal Booking
                </Text>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Tanggal: </Text>
                  <Text note>{moment(data.schedule.open).format('dddd, DD MMMM YYYY')}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5, flexWrap: 'wrap'}}>
                  <Text style={{fontWeight: 'bold'}}>Mulai Jam: </Text>
                  <Text note>{moment(data.schedule.start, 'HH:mm A').format('HH:mm A')}</Text>
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 5, flexWrap: 'wrap'}}>
                  <Text style={{fontWeight: 'bold'}}>Berkahir Jam: </Text>
                  <Text note>{moment(data.schedule.finish, 'HH:mm A').format('hh:mm A')}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>

          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Keterangan
                </Text>
                <Text>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</Text>
              </Body>
            </CardItem>
          </Card>

        </Content>

        <View style={[global.container]}>
          <Button block rounded disabled={_btnLoading} onPress={() => this.onSubmit()} loading={true}>
            <Text style={{fontSize: 16}}>Booking Sekarang</Text>
          </Button>
        </View>
      </Container>
    )
  }

  onSubmit(){
    const { actionsetBookingPsikolog, route: { params: { data } }, navigation } = this.props;
    var payload = {
      "doctorId": data.schedule.doctorId,
      "scheduleId": data.schedule.uid
    }
    this.setState({ _btnLoading: true }, () => {
      return actionsetBookingPsikolog(payload, () => {
        this.setState({ _btnLoading: false }, () => {
          navigation.goBack()
          return ToastAndroid.show('Booking Berhasil, silahkan cek notifikasi anda sekarang', ToastAndroid.SHORT);
        })
      }, (err) => {
        this.setState({ _btnLoading: false }, () => {
          return ToastAndroid.show(err.message, ToastAndroid.SHORT)
        })
      })
    })
  }

}



const mapStateToProps = (state) => ({
  getAuth: state.auth
})

const mapDispatchToProps = {
  actionsetBookingPsikolog: setBookingPsikolog
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmationKonseling)
