import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ToastAndroid, View, TouchableNativeFeedback } from 'react-native';
import { setDetailPsikolog, unmountDetailPsikolog } from '../../../../redux/actions/test/konseling/KonselingTestAction'
import { Container, Content, Card, CardItem, Thumbnail, Text, Icon, Left, H3, Body, Right } from 'native-base';
import { Loading } from '../../../components';
import moment from 'moment';

class DetailKonseling extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount(){
    const { actionsetDetailPsikolog, navigation } = this.props;
    const { params: { data } } = this.props.route;
    return actionsetDetailPsikolog(data.uid, () => null, (err) => {
      ToastAndroid.show(err.message, ToastAndroid.SHORT);
      return navigation.goBack()
    })
  }
  
  render() {
    const { getData: { data, loading } } = this.props;
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Content padder>
          <Card style={{flex: 0}}>
            <CardItem>
              <Left>
                <Thumbnail source={require('../../../img/avatar-default.png')} />
                <Body>
                  <H3>{`${data.fullName}`}</H3>
                  <Text note>{data.doctorContact ? data.doctorContact.specialist : '-'}</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem>
              <Text style={{fontSize: 15}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has</Text>
            </CardItem>
          </Card>
          
          <View style={{marginTop: 30, marginBottom: 10}}>
            <H3>List Jadwal</H3>
          </View>
          
          {data.doctorSchedule && data.doctorSchedule.map((res, i) => {
            return(
              <Card key={i}>
                <TouchableNativeFeedback onPress={() => this.onSubmit(res, data)}>
                  <CardItem>
                    <Icon active name="ios-calendar" />
                    <View style={{flex: 1}}>
                      <H3 style={{fontSize: 15}}>{moment(res.open).format('dddd, DD MMM YYYY')}</H3>
                      <Text note>Pukul {moment(res.start, 'HH:mm A').format('HH:mm A')} - {moment(res.finish, 'HH:mm A').format('HH:mm A')}</Text>
                    </View>
                    <Right>
                      <View style={{flexDirection: 'row'}}>
                        {i === 1 && <Text style={{fontSize: 12, marginRight: 15, color: 'red'}}>Booked</Text>}
                        <Icon name="ios-arrow-forward" />
                      </View>
                    </Right>
                  </CardItem>
                </TouchableNativeFeedback>
              </Card>
            )
          })}
        </Content>
      </Container>
    )
  }

  onSubmit(res, data){
    const { navigation, getAuth: { dataUser } } = this.props;
    if(dataUser.strength.includes('faceRecog') ||  dataUser.strength.includes('contacts')){
      return ToastAndroid.show('Lengkapi Biodata Anda Sekarang!', ToastAndroid.SHORT);
    }else{
      return navigation.navigate('ConfirmationKonselingPage', {
        data: {
          schedule: res,
          doctor: data
        }
      });
    }
  }
  
  componentWillUnmount(){
    const { actionunmountDetailPsikolog } = this.props;
    return actionunmountDetailPsikolog();
  }
}

const mapStateToProps = (state) => ({
  getData: state.test.konseling.detail,
  getAuth: state.auth
})

const mapDispatchToProps = {
  actionsetDetailPsikolog: setDetailPsikolog,
  actionunmountDetailPsikolog: unmountDetailPsikolog,
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailKonseling)
