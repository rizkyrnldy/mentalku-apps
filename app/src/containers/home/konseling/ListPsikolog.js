import React, { Component } from 'react'
import { ToastAndroid, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { setListPsikolog, unmountListPsikolog } from '../../../../redux/actions/test/konseling/KonselingTestAction'
import { Container, Content, Header, List, ListItem, Thumbnail, Text, Right, Left, Body, Item, Icon, Input, H3 } from 'native-base';
import { Loading } from '../../../components';
import debounce from 'lodash.debounce';

class ListPsikolog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _loadingSearch: false,
      _current: 1

    }
    this.onSearch = debounce(this.onSearch.bind(this), 500);
  }

  componentDidMount(){
    const { actionsetListPsikolog, navigation } = this.props;
    const { _current } = this.state;
    return actionsetListPsikolog("", _current, () => null, (err) => {
      ToastAndroid.show(err.message, ToastAndroid.SHORT);
      return navigation.goBack()
    });
  }

  render() {
    const { navigation, getData: {data, loading} } = this.props;
    const { _loadingSearch } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input 
              placeholder="Cari nama dokter atau spesialisasi" 
              onChangeText={this.onSearch}
            />
            {_loadingSearch && <ActivityIndicator style={[{ marginRight: 10 }]} size="small" color="#15aa1a" />}
          </Item>
        </Header>
        <Content style={{backgroundColor: '#fff'}}>
          {data.length > 0 && data.map((res, i) => {
            return(
              <List key={i} style={{}}>
                <ListItem avatar button onPress={() => navigation.navigate('DetailKonselingPage', { data: res })} >
                  <Left>
                    <Thumbnail source={require('../../../img/avatar-default.png')} />
                  </Left>
                  <Body>
                    <Text style={{fontSize: 18, fontWeight: 'bold', color: '#39393b', marginBottom: 10}}>{`${res.fullName}`}</Text>
                    <Text note style={{fontStyle: 'italic', color: '#39393b'}}>{res.doctorContact ? res.doctorContact.specialist : '-'}</Text>
                  </Body>
                  <Right style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Icon name="ios-arrow-forward" />
                  </Right>
                </ListItem>
              </List>
            )
          })}
        </Content>
      </Container>
    )
  }

  onSearch = (e) => {
    const { actionsetListPsikolog, navigation } = this.props;
    return this.setState({ _loadingSearch: true }, () => {
      return actionsetListPsikolog(e, 1, () => {
        return this.setState({ _loadingSearch: false });
      }, (err) => {
        return this.setState({ _loadingSearch: false }, () => {
          ToastAndroid.show(err.message, ToastAndroid.SHORT);
          return navigation.goBack()
        })
      });
    });
  }

  componentWillUnmount(){
    const { actionseunmountListPsikolog } = this.props;
    return actionseunmountListPsikolog();
  }
}

const mapStateToProps = (state) => ({
  getData: state.test.konseling.list
})

const mapDispatchToProps = {
  actionsetListPsikolog: setListPsikolog,
  actionseunmountListPsikolog: unmountListPsikolog,
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPsikolog)
