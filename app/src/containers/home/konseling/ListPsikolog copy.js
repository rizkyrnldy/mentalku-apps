import React, { Component } from 'react'
import { ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import { setListPsikolog, unmountListPsikolog } from '../../../../redux/actions/test/konseling/KonselingTestAction'
import { Container, Content, Card, Header, CardItem, Thumbnail, Text, Button, Left, Body, Item, Icon, Input, Toast } from 'native-base';
import { Loading } from '../../../components';

class ListPsikolog extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount(){
    const { actionsetListPsikolog, navigation } = this.props;
    var value = {
      "userType": 3,
      "current": 1, 
      "filter":{ 
        "type": "name", 
        "search": "", 
        "orderBy": "desc" 
      } 
    }
    return actionsetListPsikolog(value, () => null, () => {
      ToastAndroid.show('Get data failed', ToastAndroid.SHORT);
      return navigation.goBack()
    });
  }

  render() {
    const { navigation, getData: {data, loading} } = this.props;
    if(loading){
      return <Loading></Loading>
    }
    return (
      <Container>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Cari Psikolog" />
          </Item>
        </Header>
        <Content padder>
          {data.length > 0 && data.map((res, i) => {
            return(
              <Card style={{flex: 0}} key={i}>
                <CardItem>
                  <Left>
                    <Thumbnail source={require('../../../img/avatar-default.png')} />
                    <Body>
                      <Text>{`${res.doctorContact.name}`}</Text>
                      <Text note>Cinta Kasih Sayang</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem>
                  <Text style={{fontSize: 15}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has</Text>
                </CardItem>
                <Button full onPress={() => navigation.navigate('DetailKonselingPage', { data: res })}>
                  <Text>Konsultasi Sekarang</Text>
                </Button>
              </Card>
            )
          })}
        </Content>
      </Container>
    )
  }

  componentWillUnmount(){
    const { actionseunmountListPsikolog } = this.props;
    return actionseunmountListPsikolog();
  }
}

const mapStateToProps = (state) => ({
  getData: state.test.konseling.list
})

const mapDispatchToProps = {
  actionsetListPsikolog: setListPsikolog,
  actionseunmountListPsikolog: unmountListPsikolog,
}

export default connect(mapStateToProps, mapDispatchToProps)(ListPsikolog)
