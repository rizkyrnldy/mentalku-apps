import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, ToastAndroid, RefreshControl } from 'react-native';
import { Header, Loading } from '../../../components';
import { Container, Content, Card, List, ListItem, Thumbnail, Text, Left, Body, Button, H3 } from 'native-base';
import { setListNews, setDetailNews, setLoadMore } from '../../../../redux/actions/news/newsAction';
import { global } from "../../../styles";
import moment from 'moment';
import HTMLView from 'react-native-htmlview';

class ListNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      _btnDisable: false,
      _loading: false,
      per_page: 10,
      offset: 5
    };
  }

  componentDidMount() {
    return this.getData()
  }

  getData(){
    const { acctionsetListNews } = this.props;
    var payload = {
      per_page: this.state.per_page,
    }
    return acctionsetListNews(payload);
  }
  
  render() {
    const { getData: {data, loading} } = this.props;
    const { _btnDisable, _loading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <Container style={[global.bgWhite]}>
        <Content refreshControl={<RefreshControl refreshing={_loading}  onRefresh={() => this.onRefresh()} />}>
          <View>
            <List>
              {data.map((res, i) => {
                return(
                  <ListItem thumbnail key={i} button onPress={() => this.goToDetail(res)} >
                    <Left>
                      <Thumbnail 
                        square 
                        source={{ uri: res.fimg_url }} 
                      />
                    </Left>
                    <Body style={{paddingRight: 10}}>
                      <HTMLView 
                        value={res.title.rendered} 
                        renderNode={this.renderNode}
                      />
                      <Text note numberOfLines={1} style={{fontSize: 12, marginTop: 10}}>
                        {moment(res.date).fromNow()}
                      </Text>
                    </Body>
                  </ListItem>
                )
              })}
            </List>

            <View style={[global.centered, {flex: 1, paddingTop: 20, paddingBottom: 30}]}>
              <Button small rounded onPress={() => this.loadMore()} disabled={_btnDisable} style={{alignSelf: 'center'}}>
                <Text>Selanjutnya</Text>
              </Button>
            </View>

          </View>  
        </Content>
      </Container>
    );
  }

  renderNode = (node, index) => {
    return(
      <View key={index}>
        <H3 style={{fontSize: 13, color: '#333', lineHeight: 20}} numberOfLines={2}>{node.data}</H3>
      </View>
    )
  }
  
  onRefresh(){
    return this.getData()
  }

  goToDetail(res){
    const { navigation, acctionsetDetailNews } = this.props;
    return acctionsetDetailNews(null, res, () => {
      return navigation.navigate('DetailNewsPage', { data: res })
    })
  }

  componentWillUnmount(){
    const { acctionsetListNews } = this.props;
    var payload = {
      per_page: 5
    }
    return acctionsetListNews(payload);
  }

  loadMore(){
    const { actionsetLoadMore } = this.props;
    var payload = { per_page: this.state.per_page + this.state.offset }
    return this.setState({ per_page: payload.per_page, _loading: true, _btnDisable: true }, () => {
      actionsetLoadMore(payload, () => {
        this.setState({ _loading: false, _btnDisable: false });
      }, () => {
        ToastAndroid.show("Load more failed", ToastAndroid.SHORT);
        return this.setState({ _loading: false, _btnDisable: false })
      })
    })
  }
}


const mapActionsToProps = {
  acctionsetListNews: setListNews,
  actionsetLoadMore: setLoadMore,
  acctionsetDetailNews: setDetailNews
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.news.list,
    getAuth: state.auth,
  }
}
export default connect( mapStateToProps, mapActionsToProps )(ListNews);