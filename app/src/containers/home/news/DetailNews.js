import React, { Component } from 'react'
import { View, Image, PixelRatio, Dimensions } from 'react-native'
import { Container, Content, Text, H3, Icon } from 'native-base';
import { connect } from 'react-redux'
import { global, html } from '../../../styles'
import { unmountDetailNews } from '../../../../redux/actions/news/newsAction';
import { Loading } from '../../../components';
import HTMLView from 'react-native-htmlview';
import moment from 'moment';

const { width } = Dimensions.get('window');

export class DetailNews extends Component {
  componentDidMount() {
    
  }
    
  render() {
    const { getData: { data, loading }, route: {params} } = this.props;
    const htmlText = params.data.content.rendered.replace(/(\r\n|\n|\r|<\s*figure.*?>|<\/figure>)/gm, '');
    return (
      <Container style={[global.bgWhite]}>
        <Content padder>
          <View style={{marginBottom: 20}}>
            {/* <H3>{params.data.title.rendered}</H3> */}
            <HTMLView 
              value={params.data.title.rendered} 
              stylesheet={html}
              renderNode={this.renderNodeTitle}
            />
            <View style={{flexDirection: 'row', marginTop: 5}}>
              <Icon type="AntDesign" name="clockcircleo" style={{fontSize: 14, marginRight: 5, marginTop: 2}}/>
              <Text note>
                {moment(params.data.date).fromNow()}
              </Text>
            </View>
          </View>
          
          <HTMLView 
            stylesheet={html}
            value={htmlText} 
            renderNode={this.renderNode}
            textComponentProps={{ style: html.defaultStyle }}
          />

        </Content>
      </Container>
    )
  }

  renderNodeTitle(node, index, siblings, parent, defaultRenderer) {
    return(
      <View key={index}>
        <H3>{node.data}</H3>
      </View>
    )
  }

  renderNode(node, index, siblings, parent, defaultRenderer) {
    
    if (node.name == 'img') {
      const { src, height } = node.attribs;
      return (
        <Image
          key={index}
          style={{ width: width - 40, height: 200, marginBottom: 20 }}
          source={{ uri: src }} />
      );
    }
  }

  componentWillUnmount(){
    const { actionunmountDetailNews } = this.props;
    return actionunmountDetailNews();
  }
}

const mapStateToProps = (state) => ({
  getData: state.news.detail,
})

const mapDispatchToProps = {
  actionunmountDetailNews: unmountDetailNews
}

export default connect(mapStateToProps, mapDispatchToProps)(DetailNews)
