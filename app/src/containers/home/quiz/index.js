import React, { Component } from 'react'
import { View, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import { setListQuiz, setAnswerQuiz, sendResultQuiz } from '../../../../redux/actions/test/quiz/quizAction';
import { Loading, Radio } from '../../../components';
import { global } from '../../../styles';
import { Container, Content, Text, Button, H3, Icon, Badge, Card, } from 'native-base';
import CountDown from 'react-native-countdown-component';
import AsyncStorage from '@react-native-async-storage/async-storage';
import moment from 'moment';
// import { extendMoment } from 'moment-range';
// const moment = extendMoment(Moment);

export class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
       _answer: null,
       _timer: null,
       _num: 1,
       _loading: true,
       _btnLoading: false,
    }
  }
  
  async componentDidMount() {
    // AsyncStorage.removeItem('answerQuiz');
    const { actionsetListQuiz, getData: { data } } = this.props;
    const { navigation } = this.props.passProps;
    navigation.setOptions({ title: 'Soal Psikologi' });
    return actionsetListQuiz(null, (res) => {
      console.log(555, res.body);
      var start = moment(res.body.simUpdate).format();
      var end = moment(start).add(15, 'minutes').format();
      var now = moment().format();
      const range = moment(end).diff(now, 'seconds');
      return this.setState({
        _timer: range,
        _loading: false
      });
    });
  }

  async componentWillReceiveProps(){
    const answerQuiz = await AsyncStorage.getItem('answerQuiz');
    this.setState({
      _num: answerQuiz ? JSON.parse(answerQuiz).current + 1 : 1,
    })
  }

  render() {
    const { getData: { data, loading } } = this.props;
    const { _answer, _timer, _num, _btnLoading, _loading } = this.state;
    return (
      <Container style={[global.bgWhite]}>
        {_loading ? null : 
           <Badge success={_timer < 180 ? false : true} style={{flexDirection: 'row', marginTop: 30, alignSelf: 'center', }}>
            <View style={{flexDirection: 'row'}}>
              <Icon name="clockcircleo" type="AntDesign" style={{fontSize: 15, marginTop: 6, color: '#fff'}} />
              <CountDown
                until={_timer}
                onFinish={() => this.onFinish()}
                onChange={(e) => this.setState({_timer: e})}
                digitStyle={{backgroundColor: 'transparent', width: 25}}
                digitTxtStyle={{color: '#fff'}}
                showSeparator
                separatorStyle={{color: '#fff'}}
                timeToShow={['M', 'S']}
                timeLabels={{m: null, s: null}}
                style={{padding: 0, margin: 0, height: 15}}
                size={15}
              />
            </View>
          </Badge>
        }
        {loading ? <Loading /> : 
          <Content padder>
            <Card style={{flex: 0, borderRadius: 20, overflow: 'hidden', padding: 20}}>
              <H3>{_num}. {data.question}</H3>
              <View style={{flexDirection: 'row', marginTop: 30}}>
                <View style={{flexDirection: 'row', marginRight: 20}}>
                  <Button transparent block style={{ height: 0, paddingTop: 10, paddingBottom: 20}} onPress={() => this.setState({_answer: 1})}>
                    <Radio active={_answer === 1} /> 
                    <Text style={{fontSize: 14}}>YA</Text>
                  </Button>
                </View>
                
                <View style={{flexDirection: 'row'}}>
                  <Button transparent block style={{ height: 0, paddingTop: 10, paddingBottom: 20}} onPress={() => this.setState({_answer: 0})}>
                    <Radio active={_answer === 0} /> 
                    <Text style={{fontSize: 14}}>TIDAK</Text>
                  </Button>
                </View>
              </View>
            </Card>
            <View style={[{marginTop: 30}]}>
              <Button full rounded onPress={() => this.onSubmit()} disabled={_btnLoading || _answer === null ? true : false}>
                <Text>Jawab</Text>
              </Button>
            </View>
          </Content>
          }

      </Container>
    )
  }

  onFinish(){
    ToastAndroid.show("Waktu telah habis", ToastAndroid.SHORT);
    return this.onSubmit()
  }

  onSubmits(){
    const { acctionsendResultQuiz } = this.props;
    const { getData, navigation } = this.props.passProps;
    var _payload = {
      adminId: getData.data.userSim[0].adminId,
      result: 31,
      rawAnswer: JSON.stringify({"current":2,"question":[{"idQuiz":"MTL::QUIZ::d2923fde-0f04-498d-80de-af06bdbc9ef0","answer":1,"result":true},{"idQuiz":"MTL::QUIZ::b5bf5d26-1db9-4253-93f5-581c6d9cb0d5","answer":1,"result":true}]})
    }
    return acctionsendResultQuiz(_payload, (_res) => {
      navigation.replace('ResultQuizPage', {
        data: _res
      })
    }) 
  }

  onSubmit(){
    const { getData: { data }, actionsetAnswerQuiz, actionsetListQuiz, acctionsendResultQuiz } = this.props;
    const { getData, navigation } = this.props.passProps;
    const { _answer, _timer } = this.state;
    var payload = {
      idQuiz: data.uid,
      answer: _answer,
      result: _answer === data.answer
    };
    return actionsetAnswerQuiz(payload, (newParsing) => {
      return this.setState({_answer: null}, () => {
        // if(newParsing.current < 2 && _timer >= 0){
        if(newParsing.current < 30 && _timer >= 0){
          return actionsetListQuiz();  
        }else{
          this.setState({_btnLoading: true}, () => {
            var _result = newParsing.question.filter(element => element.result === true);
            console.log(555, 'push api', newParsing);
            var _payload = {
              adminId: getData.data.userSim[0].adminId,
              result: _result.length,
              rawAnswer: JSON.stringify(newParsing)
            }
            return acctionsendResultQuiz(_payload, (_res) => {
              navigation.replace('ResultQuizPage', {
                data: _res
              })
            }) 
          }) 
        }
      })
    })
  }
  componentWillUnmount(){
    console.log(55, 'back');
  }
  
}

const mapStateToProps = (state) => ({
  getData: state.quiz.quiz
})

const mapDispatchToProps = {
  actionsetListQuiz: setListQuiz,
  actionsetAnswerQuiz: setAnswerQuiz,
  acctionsendResultQuiz: sendResultQuiz
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
