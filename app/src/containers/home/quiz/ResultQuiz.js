import React, { Component } from 'react'
import { View } from 'react-native'
import { global } from '../../../styles';
import { Container, Content, Text, Button, H3, Icon } from 'native-base';

export default class ResultQuiz extends Component {
  render() {
    const { navigation, route: { params } } = this.props;
    return (
      <Container style={[global.bgWhite]}>
        <Content contentContainerStyle={{flex: 1}} padder>

          <View style={[global.centered]}>
            <View>
              <H3 style={{textAlign: 'center'}}>
                {params.data.graduated === 'LULUS' ? 'Selamat Anda Lulus' : 'Maaf Anda Tidak Lulus'}
              </H3>
            </View>
            
            <View>
              <Icon name={params.data.graduated === 'LULUS' ? "checkcircle" : "closecircle"} type="AntDesign" style={{color: params.data.graduated === 'LULUS' ? '#15aa1a' : '#ff0009', fontSize: 100, textAlign: 'center', marginTop: 20, marginBottom: 20}} />
            </View>

            <View>
              <H3 style={{textAlign: 'center'}}>
                {params.data.graduated === 'LULUS' ? 
                    "Silahkan datang ke gerai yang anda pilih untuk melakukan pembayaran dan mengambil blanko kelulusan anda" :
                    params.data.isDone >= 1 ?  `Silahkan datang ke gerai yang anda pilih (${params.data.outlet.outletContacts.name}) untuk melanjutkan test psikologi SIM secara manual`  :
                      "Silahkan test ulang kembali, karena anda masih ada 1 kesempatan untuk mengulang test kembali" 
                }
              </H3>
            </View>
            <Button rounded block style={{marginTop: 50}} onPress={() => navigation.goBack()}>
              <Text>Kembali</Text>
            </Button>
          </View>
        </Content>
      </Container>
    )
  }
}
