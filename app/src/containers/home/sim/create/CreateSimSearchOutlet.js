import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, ActivityIndicator, ToastAndroid } from 'react-native'
import { Empty, Loading } from '../../../../components';
import { setOutlet, setOutletDetail, unmountSimOutlet } from '../../../../../redux/actions/sim/SimAction';
import { Container, Content, Text, Header, List, ListItem, Item, Icon, Input, Left, Body, Right, Title, Button, H3 } from 'native-base';
import debounce from 'lodash.debounce';

class CreateSimSearchOutlet extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       _loadingSearch: false
    }
    this.onSearchDelayed = debounce(this.onSearch.bind(this), 500);
  }

  componentDidMount() {
    const { actionsetOutlet } = this.props;
    return actionsetOutlet("");
  }
  
  
  render() {
    const { _loadingSearch } = this.state;
    const { getData: {list, loading} } = this.props
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input 
              placeholder="Cari Domisili" 
              onChangeText={(e) => this.onSearch(e)}
            />
            {_loadingSearch && <ActivityIndicator style={[{ marginRight: 10 }]} size="small" color="#15aa1a" />}
          </Item>
        </Header>

        <Content style={{backgroundColor: '#fff'}}>
          {list.data ? list.data.map((res, i) => {
            return(
              <List key={i}>
                <ListItem onPress={() => this.onSubmit(res)} button>
                  <Left style={{flexDirection: 'column'}}>
                    <H3 style={{fontSize: 15}}>{res.outletContacts.name}</H3>
                    <View style={{flexDirection: 'row', marginLeft: -5}}>
                      <Icon type="MaterialCommunityIcons" name="map-marker" style={{ flex: 0, fontSize: 12, textAlign: 'center' }} />   
                      <Text note style={{ fontSize: 12, fontStyle: 'italic' }}> 
                        {`${res.outletContacts.district.name}, ${res.outletContacts.district.regencies.name}, ${res.outletContacts.district.regencies.provinces.name}`}
                      </Text>
                    </View>
                  </Left>
                  <Right>
                     <Icon name="ios-arrow-forward" />
                  </Right>
                </ListItem>
              </List>
            )
          }) : <Empty text="CARI GERAI TERDEKAT ANDA"/>}
        </Content>
      </Container>
    )
  }

  onSearch(e){
    const { actionsetOutlet } = this.props;
    return this.setState({_loadingSearch: true}, () => {
      return actionsetOutlet(e, () => {
        return this.setState({ _loadingSearch: false, });
      }, () => {
        return this.setState({ _loadingSearch: false }, () => ToastAndroid.show("Failed", ToastAndroid.SHORT));
      }); 
    })
  }

  onSubmit(res){
    const { navigation, actionsetOutletDetail } = this.props;
    if(res.outletContacts.simCategory){
      return actionsetOutletDetail(res, () => {
        return navigation.navigate('CreateSimTypePricePage');
      })
    }else{
      return ToastAndroid.show("Price Kategori belum di buat", ToastAndroid.SHORT)
    }
  }

  componentWillUnmount(){
    const { actionunmountSimOutlet } = this.props;
    return actionunmountSimOutlet();
  }


}

const mapStateToProps = (state) => ({
  getData: state.sim.outlet,
})

const mapDispatchToProps = {
  actionsetOutlet: setOutlet,
  actionsetOutletDetail: setOutletDetail,
  actionunmountSimOutlet: unmountSimOutlet

}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSimSearchOutlet)
