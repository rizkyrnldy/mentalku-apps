import React, { Component } from 'react'
import { View, ToastAndroid } from 'react-native'
import { connect } from 'react-redux'
import { Container, Content, Card, CardItem, Radio, Text, Button, Body } from 'native-base';
import { createSimTest } from '../../../../../redux/actions/sim/SimAction';
import { global } from '../../../../styles';
import { Loading } from '../../../../components';

class ConfirmationSim extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false,
    }
  }
  
  render() {
    const { getData: { detail, loading }, getSubmit, getAuth: { dataUser } } = this.props;
    const { _btnLoading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Content padder>
          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Data Diri
                </Text>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Nama Lengkap: </Text>
                  <Text note>{dataUser.fullName}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Email: </Text>
                  <Text note>{dataUser.email}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>No. Telepon: </Text>
                  <Text note>{dataUser.phoneNumber}</Text>
                </View>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>NIK: </Text>
                  <Text note>{dataUser.nik}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>

          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Data Gerai
                </Text>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Nama Gerai: </Text>
                  <Text note>{detail.outletContacts.name}</Text>
                </View>

                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Nama Admin Gerai: </Text>
                  <Text note>{detail.fullName}</Text>
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 5, flexWrap: 'wrap'}}>
                  <Text style={{fontWeight: 'bold'}}>Lokasi Gerai: </Text>
                  <Text note>{`${detail.outletContacts.district.name}, ${detail.outletContacts.district.regencies.name}, ${detail.outletContacts.district.regencies.provinces.name}`}</Text>
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 5, flexWrap: 'wrap'}}>
                  <Text style={{fontWeight: 'bold'}}>Alamat Gerai: </Text>
                  <Text note>{detail.outletContacts.address}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>

          
          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  Data Pemesanan
                </Text>
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Kategori SIM: </Text>
                  <Text note>SIM {getSubmit.data && getSubmit.data.category}</Text>
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Biaya Psikotest SIM: </Text>
                  <Text note>Rp {getSubmit.data && getSubmit.data.price}</Text>
                </View>
                
                <View style={{flexDirection: 'row', marginBottom: 5}}>
                  <Text style={{fontWeight: 'bold'}}>Tipe: </Text>
                  <Text note>{getSubmit.data && getSubmit.data.type === 1 ? 'Buat Baru' : "Perpanjang"}</Text>
                </View>
              </Body>
            </CardItem>
          </Card>


        </Content>

        <View style={[global.container]}>
          <Button block rounded disabled={_btnLoading} onPress={() => this.onSubmit()} loading={true}>
            <Text style={{fontSize: 16}}>
              Mulai Test
            </Text>
          </Button>
        </View>
      </Container>
    )
  }

  onSubmit(){
    const { actioncreateSimTest, navigation } = this.props;
    return actioncreateSimTest(() => {
      navigation.pop(3)
      return ToastAndroid.show('Mulai Test', ToastAndroid.SHORT);
    }, () => {
      return ToastAndroid.show('Mulai Test Gagal', ToastAndroid.SHORT)
    })
  }

}


const mapStateToProps = (state) => ({
  getData: state.sim.outlet,
  getSubmit: state.sim.submit,
  getAuth: state.auth
})

const mapDispatchToProps = {
  actioncreateSimTest: createSimTest
}

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmationSim)
