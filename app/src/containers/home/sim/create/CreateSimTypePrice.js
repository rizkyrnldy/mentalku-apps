import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, ToastAndroid,Picker } from 'react-native'
import { validationForm } from '../../../../helper';
import { setSimSubmit, unmountSimSubmit } from '../../../../../redux/actions/sim/SimAction';
import { Container, Content, Text, Item, Icon, Label, Button } from 'native-base';
import update from 'react-addons-update';
import { Loading } from '../../../../components';

class CreateSimTypePrice extends Component {
  constructor(props) {
    super(props)

    this.state = {
      _form: [
        {name: 'type', title: 'Tipe Pembuatan', value: "", type: 'picker', error: false, required: true},
        {name: 'category', title: 'Tipe Pembuatan', value: "", type: 'picker', error: false, required: true},
      ],
      _btnLoading: false,
      _loading: true,
      _simCategory: []
    }
  }

  componentDidMount(){
    const { getData: { detail } } = this.props;
    var _simCategory = Object.entries(detail.outletContacts.simCategory).filter(el => {
      return el[1] !== 0;
    });
    return this.setState({
      _simCategory: _simCategory,
      _loading: false
    })
  }
  
  render() {
    const { _form, _btnLoading, _loading, _simCategory } = this.state;
    if(_loading){
      return <Loading />
    }
    return (
      <Container>
        <Content padder style={{backgroundColor: '#fff'}}>
          <View style={{marginBottom: 20}}>
            <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>
              Pilih Tipe:
            </Label>
            <Item regular>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: '100%' }}
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state._form[0].value}
                onValueChange={(e) =>  this.setState({
                  _form: update(this.state._form, { 
                    0: { value: { $set: e }  }
                  }) 
                })}
              >
                <Picker.Item label="Pilih Tipe" />
                <Picker.Item label="Buat Baru" value={1} />
                <Picker.Item label="Perpanjang" value={2} />
              </Picker>
            </Item>
          </View>
          
          <View style={{marginBottom: 20}}>
            <Label style={{marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>
              Pilih Kategori:
            </Label>
            <Item regular>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: '100%' }}
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={`${this.state._form[1].value}`}
                onValueChange={(e) => this.setState({
                    _form: update(this.state._form, { 
                      1: { value: { $set: e }  }
                    }) 
                  })
                }
              >
                <Picker.Item label="Pilih Kategori" />
                  {_simCategory.map((res, i) => {
                    return(
                      <Picker.Item label={`SIM ${res[0]}`} value={`${res[0]}, ${res[1]}`} key={i} />
                    )
                  })}
              </Picker>
            </Item>
          </View>
          
          <Button full rounded onPress={() => this.onSubmit()} disabled={_btnLoading}>
            <Text>Lanjutkan</Text>
          </Button>
        </Content>
      </Container>
    )
  }

  onSubmit(){
    const { navigation, actionsetSimSubmit } = this.props;
    const { _form } = this.state;
    this.setState({ _btnLoading: true }, () => {
      return validationForm(_form, (res) => {
        return actionsetSimSubmit(res, () => {
          return this.setState({_btnLoading: false}, () => navigation.navigate('ConfirmationSimPage'));
        })
      }, (err) => {
        return this.setState({
          _form: err,
          _btnLoading: false
        }, () => ToastAndroid.show("Form can not be null", ToastAndroid.SHORT))
      })
    })
  }

  componentWillUnmount(){
    const { actionunmountSimSubmit } = this.props;
    return actionunmountSimSubmit();
  }

}

const mapStateToProps = (state) => ({
  getData: state.sim.outlet,
})

const mapDispatchToProps = {
  actionsetSimSubmit: setSimSubmit,
  actionunmountSimSubmit: unmountSimSubmit
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSimTypePrice)
