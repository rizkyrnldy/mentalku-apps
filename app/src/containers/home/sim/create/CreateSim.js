import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, ToastAndroid, RefreshControl, StyleSheet } from 'react-native'
import { setOutlet } from '../../../../../redux/actions/sim/SimAction';
import { Container, Content, Card, CardItem, Text, Button, Body } from 'native-base';
import { global } from '../../../../styles';
import { Radio } from '../../../../components';
import HTMLView from 'react-native-htmlview';

class CreateSim extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false,
      _radioAccept: false
    }
  }

  componentDidMount() {
    const { navigation } = this.props.passProps;
    return navigation.setOptions({
      title: 'Test Psikologi SIM',
    });
  }
  
  
  render() {
    const { getData: { data } } = this.props.passProps;
    const { _btnLoading, _radioAccept } = this.state;
    return (
      <Container>
        <Content padder refreshControl={<RefreshControl refreshing={_btnLoading} />}>  
          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <HTMLView 
                  value={data.content} 
                  stylesheet={styles}
                />
              </Body>
            </CardItem>
          </Card>
        </Content>

        <View style={[global.l_container]}>
          <Card style={[{borderRadius: 20, overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <View style={{flexDirection: 'row', marginBottom: 10}}>
                  <Button transparent block style={{ height: 0, paddingTop: 10, paddingBottom: 20}} onPress={() => this.setState({_radioAccept: !_radioAccept})}>
                    <Radio active={_radioAccept} />
                    <Text style={{fontSize: 13, marginTop: -3, color: '#062b54' }}>Setuju dengan pernyataan ini</Text>
                  </Button>
                </View>
                <Button block rounded disabled={!_radioAccept || _btnLoading } onPress={() => this.onCreate()} loading={true}>
                  <Text style={{fontSize: 16}}>
                    Mulai Test Psikologi
                  </Text>
                </Button>
              </Body>
            </CardItem>
          </Card>
        </View>

      </Container>
    )
  }


  onCreate(){
    const { getAuth: { dataUser } } = this.props;
    const { navigation } = this.props.passProps;
    if(dataUser.strength.includes('faceRecog') ||  dataUser.strength.includes('contacts')){
      return ToastAndroid.show('Lengkapi Biodata Anda Sekarang!', ToastAndroid.SHORT);
    }else{
      // return this.setState({ _btnLoading: true }, () => {
        return navigation.navigate('CreateSimSearchOutlet')
      // })
    }
  }
  
}


const styles = StyleSheet.create({
  a: {
    color: '#15aa1a'
  },

  p: {
    fontFamily: 'FiraSans',
    marginTop: 0,
    marginBottom: 0
  },
});

const mapStateToProps = (state) => ({
  // getData: state.sim.sim,
  getAuth: state.auth
})

const mapDispatchToProps = {
  actionsetOutlet: setOutlet,

}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSim)
