import React, { Component } from 'react'
import { connect } from 'react-redux'
import { checkSimTest, unmountSimTest } from '../../../../redux/actions/sim/SimAction';
import { Loading } from '../../../components';
import CreateSim from './create/CreateSim';
import ReadSim from './ReadSim';
import TestSim from './TestSim';
import ConditionalSim from './ConditionalSim';
import StartQuiz from '../quiz';

export class index extends Component {
 
  componentDidMount(){
    const { actioncheckSimTest } = this.props;
    return actioncheckSimTest();
  }
  
  render() {
    const { getData:{ status, loading }}  = this.props;
    if(loading){
      return <Loading />
    }
    console.log(19191, status, this.props);
    switch (status) {
      case 'USER_NOT_REGISTER':
        return <CreateSim passProps={this.props} />

      case 'TEST_IS_NOT_READY':
        return <TestSim passProps={this.props} />;

      case 'TEST_IS_READY': 
        return <StartQuiz passProps={this.props} />
        
      case 'SUCCESS_UNPAID':
        return <ReadSim passProps={this.props} />

      case 'CONDITIONAL_PASS': 
        return <ConditionalSim passProps={this.props} />;

      default:
        return null;
    }
  }


  componentWillUnmount(){
    const { actionunmountSimTest } = this.props;
    return actionunmountSimTest();
  }
}

const mapStateToProps = (state) => ({
  getData: state.sim.sim,
})

const mapDispatchToProps = {
  actioncheckSimTest: checkSimTest,
  actionunmountSimTest: unmountSimTest,
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
