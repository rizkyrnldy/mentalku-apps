import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View } from 'react-native'
import { Container, Content, List, ListItem, Text, Left, Right, Icon, Button, H2, Toast } from 'native-base';

class ConditionalSim extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount() {
    const { navigation } = this.props.passProps;
    return navigation.setOptions({
      title: 'Gagal Test Psikologi SIM',
    });
  }
  

  render() {
    const { navigation, getData: { data } } = this.props.passProps
    console.log(22, data.userSim[0].admin.outletContacts.name);
    return (
      <Container style={[{backgroundColor: '#fff'}]}>
        <Content contentContainerStyle={{flex: 1}} padder>
          <View style={{alignSelf: 'center', justifyContent: 'center', flex: 1, marginTop: -30}}>
            <H2 style={{textAlign: 'center'}}>MAAF ANDA BELUM LULUS</H2>
            <Text style={{textAlign: 'center', marginTop: 10, marginBottom: 40}}>
              Silahkan datang ke gerai yang anda {'\n'} pilih <Text bold>({data.userSim[0].admin.outletContacts.name})</Text> untuk melanjutkan test psikologi SIM secara manual
            </Text>
            <View style={[global.container]}>
              <Button block rounded onPress={() => navigation.goBack()}>
                <Text>KEMBALI</Text>
              </Button>
            </View>
          </View>

          {/* <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  MAAF ANDA BELUM LULUS
                </Text>
                <Text note>
                  Silahkan datang ke gerai yang anda pilih <Text bold>(Gerai hehe)</Text> untuk melanjutkan test psikologi SIM`
                </Text>
              </Body>
            </CardItem>
          </Card>
           */}
        </Content>
      </Container>
    )
  }


}

const mapStateToProps = (state) => ({
  // getData: state.sim.sim,
})

const mapDispatchToProps = {
  // actioncheckSimTest: checkSimTest,
}

export default connect(mapStateToProps, mapDispatchToProps)(ConditionalSim)
