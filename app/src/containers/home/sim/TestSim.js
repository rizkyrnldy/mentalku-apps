import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, RefreshControl, Modal, ToastAndroid, StyleSheet } from 'react-native'
import { Container, Content, Card, CardItem, Text, Button, Body } from 'native-base';
import { global } from '../../../styles';
import { Radio } from '../../../components';
import { checkSimTest } from '../../../../redux/actions/sim/SimAction';
import { startQuiz } from '../../../../redux/actions/test/quiz/quizAction';
import CheckFaceRecog from '../../face/CheckFaceRecog';
import { Camera } from 'expo-camera';
import HTMLView from 'react-native-htmlview';

class TestSim extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _radioAccept: false,
      _btnLoading: false,
      _modalVisible: false,
      _hasCameraPermission: null,
    }
  }

  componentDidMount() {
    const { navigation } = this.props.passProps;
    return navigation.setOptions({
      title: 'Mulai Test',
    });
  }
  
  
  render() {
    const { getData: { data } } = this.props;
    const { _btnLoading, _radioAccept, _modalVisible } = this.state;
    return (
      <Container>
        <Content padder refreshControl={<RefreshControl refreshing={_btnLoading} />}>
          <Card style={[{overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <HTMLView 
                  value={data.content} 
                  stylesheet={styles}
                />
              </Body>
            </CardItem>
          </Card>
          
        </Content>
        
        <View style={[global.l_container]}>
          <Card style={[{borderRadius: 20, overflow: 'hidden'}]}>
            <CardItem>
              <Body>
                <View style={{flexDirection: 'row', marginBottom: 10}}>
                  <Button transparent block style={{ height: 0, paddingTop: 10, paddingBottom: 20}} onPress={() => this.setState({_radioAccept: !_radioAccept})}>
                    <Radio active={_radioAccept} />
                    <Text style={{fontSize: 13, marginTop: -3, color: '#062b54' }}>Setuju dengan pernyataan ini</Text>
                  </Button>
                </View>
                <Button block rounded disabled={!_radioAccept || _btnLoading } onPress={() => this.onStart()}>
                  <Text style={{fontSize: 16}}>MULAI</Text>
                </Button>
              </Body>
            </CardItem>
          </Card>
        </View>

        <Modal
          animationType="slide"
          transparent={true}
          visible={_modalVisible}
          onRequestClose={() => this.onCloseModal()}>
            <CheckFaceRecog 
              onCloseModal={() => this.onCloseModal()}
              onSubmit={(img) => this.onSubmit(img)} 
          />
        </Modal>
      </Container>
    )
  }

  async onStart(){
    // const { status } = await Permissions.askAsync(Permissions.CAMERA);
    const { status } = await Camera.requestPermissionsAsync();
    if(status === 'granted'){
      return this.setState({
        _hasCameraPermission: status === 'granted',
        _modalVisible: true,
        _radioAccept: false
      });
    }else{
      return ToastAndroid.show("Permission kamera tidak di izinkan", ToastAndroid.SHORT);
    }
  }

  onCloseModal(){
    return this.setState({ _modalVisible: false })
  }

  onSubmit(img){
    const { actionstartQuiz, actioncheckSimTest, getData } = this.props;
    console.log(222, getData);
    return this.setState({ _modalVisible: false }, () => {
      ToastAndroid.show("Harap Tunggu", ToastAndroid.SHORT);
      var payload = {
        image: img,
        adminId: getData.data.userSim[0].adminId,
      }
      return actionstartQuiz(payload, (res) => {
        return this.setState({ _btnLoading: false }, () => {
          ToastAndroid.show(res.message, ToastAndroid.SHORT)
          return actioncheckSimTest();
        })
      }, (err) => {
        return this.setState({ _btnLoading: false }, () => ToastAndroid.show(err.message, ToastAndroid.SHORT))
      })
    })
  }

}


const styles = StyleSheet.create({
  a: {
    color: '#15aa1a'
  },

  p: {
    fontFamily: 'FiraSans',
    marginTop: 0,
    marginBottom: 0
  },
});


const mapStateToProps = (state) => ({
  getData: state.sim.sim,
})

const mapDispatchToProps = {
  actioncheckSimTest: checkSimTest,
  // actionunmountSimTest: unmountSimTest,
  // actionchecQuotaSim: checQuotaSim,
  actionstartQuiz: startQuiz
}

export default connect(mapStateToProps, mapDispatchToProps)(TestSim)
