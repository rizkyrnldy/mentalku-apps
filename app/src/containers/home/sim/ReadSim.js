import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Dimensions, ToastAndroid } from 'react-native'
import { checkSimTest } from '../../../../redux/actions/sim/SimAction';
import QRCode from 'react-native-qrcode-svg';
import { global } from '../../../styles';
import { Loading } from '../../../components';
import { Container, Content, Text, Button, H3, Icon } from 'native-base';
// import { decode } from '../../../helper'

class ReadSim extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }
  componentDidMount() {
    const { navigation } = this.props.passProps;
    return navigation.setOptions({
      title: 'Bukti Psikologi SIM',
    });
  }
  

  render() {
    const { getData: { data } } = this.props;
    if(data === null){
      return <Loading />
    }
    // const adminId = JSON.parse(decode(data)).userSim.adminId;
    // const adminId = data.admId;
    console.log(22, data);
    return (
      <Container style={[{backgroundColor: '#fff'}]}>
        <Content contentContainerStyle={{flex: 1}} padder>
          <View style={[global.centered]}>
            <View style={[global.centered, {position: 'absolute', top: 0}]}>
              <Button rounded iconLeft light onPress={() => this.onRefresh()}>
                <Icon name="md-refresh-circle" type="Ionicons" />
                <Text>Perbarui</Text>
              </Button>
            </View>
            <View style={{marginBottom: 20}}>
              <H3 style={[global.textCenter]}>QR KODE ANDA</H3>
                <Text note style={[global.textCenter]}>Tunjukan QR Kode anda ke <Text style={{fontWeight: 'bold'}}>"{data.outlet.outletContacts.name}"</Text> yang anda pilih dan Silahkan Scan QR Code anda ke QR Code reader yang telah disediakan</Text>
            </View>
            <QRCode
              value={data.userId}
              color={'#000'}
              backgroundColor={'transparent'}
              size={Dimensions.get('window').width - 40}
              logoSize={60}
              logoBorderRadius={100}
              logo={require('../../../img/logo-icon-dark.png')}
              logoBackgroundColor="#fff"
              logoMargin={10}
            />
          </View>
        </Content>
      </Container>
    )
  }

  onRefresh(){
    const { actioncheckSimTest } = this.props;
    return actioncheckSimTest(() => {
      return ToastAndroid.show('Berhasil diperbarui', ToastAndroid.LONG)
    }, () => {
      return ToastAndroid.show('Gagal diperbarui', ToastAndroid.LONG)
    });
  }


}

const mapStateToProps = (state) => ({
  getData: state.sim.sim,
})

const mapDispatchToProps = {
  actioncheckSimTest: checkSimTest,
}

export default connect(mapStateToProps, mapDispatchToProps)(ReadSim)
