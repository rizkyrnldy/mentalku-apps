import React, { Component } from 'react'
import { Text, View, StyleSheet, Vibration } from 'react-native';
import Constants from 'expo-constants';
import { Camera } from 'expo-camera';
import * as FaceDetector from 'expo-face-detector';
import * as MediaLibrary from 'expo-media-library';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { decode } from '../../../helper'

export default class Reader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      status: null,
      hasCameraPermission: null,
      hasCameraRollPermissions: null,
      faces: [],
      scanned: false
    }
  }

  async componentDidMount() {
    const { status } = await Camera.requestPermissionsAsync();
    const { cameraRollStatus } = await Camera.requestPermissionsAsync();
    // const { status } = await Permissions.askAsync(Permissions.CAMERA);
    // const { cameraRollStatus } = await Permissions.askAsync(Permissions.CAMERA_ROLL)
    this.setState({
      hasCameraPermission:status === 'granted',
      hasCameraRollPermissions:cameraRollStatus === 'granted',
    });
  }
  
  handleBarCodeScanned = ({ type, data }) => {
    const { navigation } = this.props;
    console.log(555, navigation);
    if(data){
      // this.setState({ scanned: true }, () => {
        var _data = JSON.parse(decode(data));
        // Vibration.vibrate();
        return navigation.navigate('ReaderDetailPage', {
          data: _data
        })
      // })
    }
  }
  
  render() {
    const { faces, faceDetecting, scanned } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
          style={{flex: 1}}
        />
      </View>
    )
  }
  renderFace({ bounds, faceID, rollAngle, yawAngle }) {
    return (
      <View
        key={faceID}
        transform={[
          { perspective: 600 },
          { rotateZ: `${rollAngle.toFixed(0)}deg` },
          { rotateY: `${yawAngle.toFixed(0)}deg` },
        ]}
        style={[
          styles.face,
          {
            ...bounds.size,
            left: bounds.origin.x,
            top: bounds.origin.y,
          },
        ]}>
        <Text style={styles.faceText}>rollAngle: {rollAngle.toFixed(0)}</Text>
        <Text style={styles.faceText}>yawAngle: {yawAngle.toFixed(0)}</Text>
      </View>
    );
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  camera: {
    flex: 1,
    justifyContent: 'space-between',
  },
  topBar: {
    flex: 0.2,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingTop: Constants.statusBarHeight+1,
  },
  bottomBar: {
    flex: 0.2,
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  face: {
    padding: 10,
    borderWidth: 1,
    borderRadius: 1,
    position: 'absolute',
    borderColor: '#32CD32',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  faceText: {
    color: '#32CD32',
    fontWeight: 'bold',
    textAlign: 'center',
    margin: 10,
    backgroundColor: 'transparent',
  },
  facesContainer: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    top: 0,
  },
  textcolor:{
    color: '#008080',
  }
});
