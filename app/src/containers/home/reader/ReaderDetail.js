import React, { Component } from 'react'
import { View } from 'react-native'
import { Text } from 'native-base';

export default class ReaderDetail extends Component {
  render() {
    const { route: { params: {data} } } = this.props;
    return (
      <View style={{margin: 20}}>
        <Text h5 style={{marginBottom: 20}}> {data.email} </Text>
        <Text h5 style={{marginBottom: 20}}> {data.uid} </Text>
        <Text h5 style={{marginBottom: 20}}> {data.userType} </Text>
        <Text h5 style={{marginBottom: 20}}> ------- </Text>
        <Text h5 style={{marginBottom: 20}}> address: {data.user_contacts ? data.user_contacts.address : 'Kosong'} </Text>
        <Text h5 style={{marginBottom: 20}}> city: {data.user_contacts ? data.user_contacts.city : 'Kosong'} </Text>
        <Text h5 style={{marginBottom: 20}}> firstName: {data.user_contacts ? data.user_contacts.firstName : 'Kosong'} </Text>
        <Text h5 style={{marginBottom: 20}}> lasttName: {data.user_contacts ? data.user_contacts.lasttName : 'Kosong'} </Text>
      </View>
    )
  }
}
