import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RefreshControl, Platform, Alert } from 'react-native';
import { Header, Loading } from '../../components';
import { Container, Content } from 'native-base';
import { MainFitur, Promo, News } from './partialHome';
import { setListNews, setDetailNews } from '../../../redux/actions/news/newsAction';

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    return this.getData()
  }

  getData(){
    const { acctionsetListNews } = this.props;
    var payload = {
      per_page: 4,
    }
    return acctionsetListNews(payload);
  }

  render() {
    const { getAuth: { dataUser }, getData: { loading } } = this.props;
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Header show={true} title={`Hi, ${dataUser.fullName ? dataUser.fullName : dataUser.phoneNumber}`} /> 
        <Content refreshControl={<RefreshControl refreshing={false}  onRefresh={() => this.onRefresh()} />}>
          <MainFitur passProps={this.props} />
          {/* <Promo passProps={this.props} /> */}
          <News passProps={this.props} />
        </Content>
      </Container>
    );
  }


  onRefresh(){
    return this.getData()
  }

}


const mapActionsToProps = {
  acctionsetListNews: setListNews,
  acctionsetDetailNews: setDetailNews,
}

const mapStateToProps = (state, props) => {
  return {
    getData: state.news.list,
    getAuth: state.auth,
  }
}
export default connect( mapStateToProps, mapActionsToProps )(Home);