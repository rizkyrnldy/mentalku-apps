import React, { Component } from 'react'
import { View, Image, StyleSheet } from 'react-native';
// import { Container, Content, Card, CardItem, List, ListItem, Thumbnail, Icon, Text, Left, Body, Right, Button, H3 } from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
// import { Col, Grid, Row } from "react-native-easy-grid";
import { global } from "../../../styles";
import Swiper from 'react-native-swiper'

export default class Promo extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       _slide: [
        require('../../../img/promo/slide-1.png'),
        require('../../../img/promo/slide-2.png'),
        // require('../../../img/promo/slide-3.png')
       ]
    }
  }
  

  render() {
    // const { navigation } = this.props.passProps;
    const { _slide } = this.state;
    return (
     <View style={[global.container]}>
       <Swiper 
        autoplay={true} 
        height={150}
        paginationStyle={{
          bottom: 0,
          marginBottom: 0
        }}
        showsButtons={false} 
        activeDotStyle={[styles.activeDotStyle]} 
        dotStyle={[styles.dotStyle]} >
          {_slide.map((res, i) => {
            return(
              <View key={i} style={{backgroundColor: '#fff', padding: 10, borderRadius: 10}}>
                <TouchableNativeFeedback>
                  <Image 
                    source={res}
                    style={{width: null, height: 100}}
                  />
                </TouchableNativeFeedback>
              </View>
            )
          })}
        </Swiper>
     </View>
    )
  }
}


const styles = StyleSheet.create({
  activeDotStyle: {
    width: 10, 
    height: 10, 
    borderRadius: 100, 
    backgroundColor:'#15aa1a', 
    // marginLeft: 10, 
    // marginRight: 10, 
    borderWidth: 1, 
    borderColor: '#707070' 
  },
  dotStyle: {
    width: 10, 
    height: 10, 
    borderRadius: 100, 
    // marginLeft: 10, 
    // marginRight: 10,  
    borderWidth: 1, 
    backgroundColor:'#fff',
    borderColor: '#707070'
  }
})