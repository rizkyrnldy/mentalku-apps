import MainFitur from './MainFitur';
import Promo from './Promo';
import News from './News';

export {
  MainFitur,
  Promo,
  News
}