import React, { Component } from 'react'
import { View, ToastAndroid, StyleSheet } from 'react-native';
import { Card, List, ListItem, Thumbnail, Text, Left, Body, Button, H3 } from 'native-base';
import { global } from "../../../styles";
import moment from 'moment';
import HTMLView from 'react-native-htmlview';

export default class News extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      _btnDisable: false
    }
  }
  
  render() {
    const { navigation, getData: {data} } = this.props.passProps;
    const { _btnDisable } = this.state;
    return (
      <View>
        <Card style={[global.bgWhite, {borderRadius: 20, overflow: 'hidden'}]}>
          <View style={[{flexDirection: 'row', justifyContent: 'space-between', padding: 20, paddingBottom: 0}]}>
            <View>
              <H3>Artikel</H3>
            </View>
            <View>
              <Button transparent onPress={() => navigation.navigate('ListNewsPage')} style={{height: 0}}>
                <H3 style={{fontSize: 13, color: '#15aa1a', fontStyle: 'italic'}}>Lihat Semua</H3>
              </Button>
            </View>
          </View>
          <List>
            {data.map((res, i) => {
              return(
                <ListItem thumbnail key={i} button onPress={() => this.goToDetail(res)} >
                  <Left>
                    <Thumbnail 
                      square 
                      source={{ uri: res.fimg_url }} 
                    />
                  </Left>
                  <Body style={{paddingRight: 10}}>
                    <HTMLView 
                      value={res.title.rendered} 
                      stylesheet={styles}
                      renderNode={this.renderNode}
                    />
                    <Text note numberOfLines={1} style={{fontSize: 12, marginTop: 10}}>
                      {moment(res.date).fromNow()}
                    </Text>
                  </Body>
                </ListItem>
              )
            })}
          </List>
        </Card>  

        <View style={[global.centered, {flex: 1, paddingTop: 20, paddingBottom: 30}]}>
          <Button small rounded onPress={() => navigation.navigate('ListNewsPage')} disabled={_btnDisable} style={{alignSelf: 'center'}}>
            <Text>Lihat Semua</Text>
          </Button>
        </View>

      </View>  
    )
  }

  renderNode = (node, index) => {
    return (
      <View key={index}>
        <H3 style={{fontSize: 13, color: '#333'}} numberOfLines={2}>{node.data}</H3>
      </View>
    );
  }

  goToDetail(res){
    const { navigation, acctionsetDetailNews } = this.props.passProps;
    return acctionsetDetailNews(null, res, () => {
      return navigation.navigate('DetailNewsPage', { data: res })
    })
  }
}


const styles = StyleSheet.create({
 
});