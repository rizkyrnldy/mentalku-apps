import React, { Component } from 'react'
import { View, Image } from 'react-native';
import { Card, CardItem, Text, Toast } from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { Col, Grid, Row } from "react-native-easy-grid";
import { global } from "../../../styles";

export default class MainFitur extends Component {
  render() {
    const { navigation } = this.props.passProps;
    return (
      <View style={[global.container, { marginTop: 20 }]}>
        <Grid>
          <Row>
            <Col style={{margin: 5}}>
              <Card style={{borderRadius: 20, overflow: 'hidden'}}>
                <TouchableNativeFeedback onPress={() => navigation.navigate('SimPage')}>
                  <CardItem>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <View>
                        <Image source={require('../../../img/home/sim.png')}  style={{ width: 70, height: 70 }} />
                      </View>
                      <View style={{marginTop: 10}}>
                        <Text style={{ color: '#062b54', fontSize: 14}} numberOfLines={1}>
                          Test Psikologi SIM
                        </Text>
                        <Text style={{ color: '#333', fontSize: 10, textAlign: 'center'}} numberOfLines={1}>
                          {/* (Coming Soon) */}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                </TouchableNativeFeedback>
              </Card>
            </Col>
            
            <Col style={{margin: 5}}>
              <Card style={{borderRadius: 20, overflow: 'hidden'}}>
                <TouchableNativeFeedback onPress={() => Toast.show({ text: 'Coming Soon' }) }>
                  <CardItem>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <View>
                        <Image source={require('../../../img/home/konseling.png')}  style={{ width: 70, height: 70 }} />
                      </View>
                      <View style={{marginTop: 10, flexDirection: 'column'}}>
                        <Text style={{ color: '#062b54', fontSize: 14, textAlign: 'center'}} numberOfLines={1}>
                          Konseling
                        </Text>
                        <Text style={{ color: '#333', fontSize: 10, textAlign: 'center'}} numberOfLines={1}>
                          (Coming Soon)
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                </TouchableNativeFeedback>
              </Card>
            </Col>
          </Row>
          
          <Row>
            <Col style={{margin: 5}}>
              <Card style={{borderRadius: 20, overflow: 'hidden'}}>
                <TouchableNativeFeedback onPress={() => Toast.show({ text: 'Coming Soon' }) }>
                  <CardItem>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <View>
                        <Image source={require('../../../img/home/assesment.png')}  style={{ width: 70, height: 70 }} />
                      </View>
                      <View style={{marginTop: 10, flexDirection: 'column'}}>
                        <Text style={{ color: '#062b54', fontSize: 14, textAlign: 'center'}} numberOfLines={1}>
                          Assesment
                        </Text>
                        <Text style={{ color: '#333', fontSize: 10, textAlign: 'center'}} numberOfLines={1}>
                          (Coming Soon)
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                </TouchableNativeFeedback>
              </Card>
            </Col>
            
            <Col style={{margin: 5}}>
              <Card style={{borderRadius: 20, overflow: 'hidden'}}>
                <TouchableNativeFeedback onPress={() => Toast.show({ text: 'Coming Soon' }) }>
                  <CardItem>
                    <View style={{flex: 1, alignItems: 'center'}}>
                      <View>
                        <Image source={require('../../../img/home/bpjs.png')}  style={{ width: 70, height: 70 }} />
                      </View>
                      <View style={{marginTop: 10, flexDirection: 'column'}}>
                        <Text style={{ color: '#062b54', fontSize: 14, textAlign: 'center'}}>BPJSTK</Text>
                        <Text style={{ color: '#333', fontSize: 10, textAlign: 'center'}} numberOfLines={1}>
                          (Coming Soon)
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                </TouchableNativeFeedback>
              </Card>
            </Col>
          </Row>
          
        </Grid>
      </View>  
    
    )
  }
}
