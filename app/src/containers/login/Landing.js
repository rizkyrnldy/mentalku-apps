import React, { Component } from 'react'
import { StyleSheet, View, ImageBackground, Image, } from 'react-native'
import { global } from '../../styles';
import { Container, Button, Text, H2 } from 'native-base';
import Swiper from 'react-native-swiper'
import { StatusBar } from 'expo-status-bar';
import Constants from 'expo-constants';

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false,
      _slider: [
        {title: 'Test Psikologi SIM', desc: 'Layanan Test Psikologi SIM yang dapat anda gunakan untuk pembuatan SIM baru ataupun Perpanjang'},
        {title: 'Konseling', desc: 'Layanan Konseling Psikologi yang diberikan kepada anda secara tatap muka digital yang memudahkan anda dalam mengatur waktu dimanapun dan kapanku'},
        {title: 'Corporate', desc: 'Layanan Psikotest untuk mengukur potensi karyawan di perusahaan-perusahaan dengan praktis dan efisien '},
      ]
    }
  }

  render() {
    const { _btnLoading, _slider } = this.state;
    return (
      <Container>
          <ImageBackground source={require("../../img/bg-login.png")} style={{ flex: 1, resizeMode: "cover", justifyContent: "center" }}>

            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <View>
                <View style={[global.centered, {flex: 0, marginBottom: 10}]}>
                  <Image source={require("../../img/logo-icon-light.png")} style={{width: 60, height: 77}} />
                </View>
                <View style={[global.centered, {flex: 0}]}>
                  <Image source={require("../../img/logo-font.png")} style={{width: 158, height: 30}} />
                </View>
              </View>

              <View style={{flexGrow: 2, height: 150, marginTop: 40}}>
                <Swiper 
                  autoplay={true}
                  showsButtons={false} 
                  activeDotStyle={{width: 10, height: 10, borderRadius: 100, backgroundColor:'#15aa1a', marginLeft: 10, marginRight: 10, borderWidth: 1, borderColor: '#707070' }} 
                  dotStyle={{width: 10, height: 10, borderRadius: 100, marginLeft: 10, marginRight: 10,  borderWidth: 1, borderColor: '#707070' }} 
                  paginationStyle={{
                    bottom: 0,
                  }}
                >
                  {_slider.map((res, i) => {
                    return(
                      <View key={i} style={[global.container]}>
                        <H2 style={{color: '#15aa1a', textAlign: 'center', marginBottom: 10, textTransform: 'uppercase'}}>
                          {res.title}
                        </H2>
                        <Text style={styles.desc}>
                          {res.desc}
                        </Text>
                      </View>
                    )
                  })}
                </Swiper>
              </View> 
            </View>
            <View style={[{position: 'absolute', left: 20, right: 20, bottom: 20} ]}>
              <Button rounded block onPress={() => this.goToLogin()} disabled={_btnLoading}>
                <Text style={{fontSize: 14}}>Mulai</Text>
              </Button>
              <Text style={{color: '#707070', textAlign: 'center', fontSize: 12, marginTop: 20}} >
                Mentalku V.{Constants.manifest.version}
              </Text>
            </View>
            
          </ImageBackground>
      </Container>
    )
  }

  goToLogin(){
    const { navigation } = this.props;
    return this.setState({ _modalVisible: false }, () => navigation.navigate('LoginPage'))
  }
}

const styles = StyleSheet.create({
  title: {
    color: '#fff',
    fontSize: 28,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#15aa1a',
    marginBottom: 10
  },
  desc: {
    color: '#fff',
    textAlign: 'center',
    color: '#fff'
  },
})
