import React, { Component } from 'react'
import { View, StyleSheet, Image } from 'react-native'
import { connect } from 'react-redux'
import { setVerification, resendVerification } from '../../../redux/actions/authAction';
import { Container, Content, Button, Text, Toast} from 'native-base';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { global } from '../../styles';
import CountDown from 'react-native-countdown-component';
 
export class Verification extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _code: "",
      _btnDisable: true,
      _resendCode: false,
      _timer: 60,
    }
  }

  // componentDidUpdate(nextprops, nextState){
  //   if(nextState._resendCode){
  //     this.setState({_timer: 0})
  //   }
  // }

  render() {
    const { route: { params }, navigation } = this.props;
    const { _btnDisable, _code, _resendCode, _timer} = this.state;
    return (
      <Container>
        <Content style={{marginTop: 20}} padder>
          <View style={[global.centered, {flexDirection: 'row', marginBottom: 40}]}>
            <Image source={require("../../img/verif-icon.png")} style={{width: 80, height: 84}} />
            <View style={{marginLeft: 20, width: 200, justifyContent: 'center'}}>
              <Text style={{fontWeight: 'bold', color: '#15aa1a'}}>Masukan 4 angka kode</Text>
              <Text note>Yang telah terkirim melalui {params.from === 'editProfile' ? "Email anda." : "SMS ke nomor Anda" } <Text bold>{`0${params.data.value}`} </Text></Text>
            </View>
          </View>

          <View style={[global.centered]}>
            <OTPInputView
              style={[ {width: '78%' }]}
              placeholderTextColor="red"
              pinCount={4}
              code={_code}
              onCodeChanged = {code => { 
                this.setState({ _code: code }, () => {
                  return this.setState({
                    _btnDisable: this.state._code.length < 4 ? true : false
                  })
                }) 
              }}
              autoFocusOnLoad
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
              onCodeFilled = {(code => {
                return this.setState({ _btnDisable: false }) 
              })}
            />
          </View>
          <View style={{marginTop: 20}}>
          <CountDown
            until={_timer}
            onFinish={() => this.setState({_resendCode: true})}
            onPress={() => console.log('finish')}
            digitStyle={{backgroundColor: 'transparent', width: 35}}
            digitTxtStyle={{color: '#707070'}}
            showSeparator
            separatorStyle={{color: '#707070'}}
            timeToShow={['M', 'S']}
            timeLabels={{m: null, s: null}}
            style={{padding: 0, margin: 0, height: 15}}
            size={15}
          />
          {_resendCode &&
            <Button small warning rounded onPress={() => this.resendCode()} style={{alignSelf: 'center', marginTop: 20}}>
              <Text>Kirim Ulang</Text>
            </Button>
            }
          </View>

        </Content>
        <View style={[global.container]}>
          <Button transparent full onPress={() => navigation.goBack()}>
            <Text style={{fontSize: 13}}><Text style={{fontSize: 13}}>SALAH NOMOR PONSEL? </Text>UBAH NOMOR TELEPON</Text>
          </Button>

          <Button block rounded onPress={() => this.onSubmit(_code)} disabled={_btnDisable}>
            <Text>Verifikasi</Text>
          </Button>
          <Button transparent full onPress={() => navigation.navigate('TermConditionPage')}>
            <Text>Syarat & Ketentuan</Text>
          </Button>
        </View>
      </Container>
    )
  }

  onSubmit(code){
    const { actionsetVerification, route: { params } } = this.props;
    var payload = { 
      vcid: params.data.vcid, 
      value: params.data.value, 
      code: code,
      type: 'USER'
    }
    return this.setState({ _btnDisable: true }, () => {
      return actionsetVerification(payload, () => null , () => {
        return this.setState({ _btnDisable: false }, () => {
          return Toast.show({ text: "Verifikasi kode anda salah", type: "danger" });
        })
      })
    })
  }
  
  resendCode(){
    const { actionresendVerification, route: { params } } = this.props;
    var payload = { vcid: params.data.vcid, value: params.data.value }
    return this.setState({ _resendCode: false, _timer: 60 }, () => {
      return actionresendVerification(payload, (res) => {
        return Toast.show({ text: res.status.message, type: "success"});
      } , () => {
        return this.setState({ _btnDisable: false, _resendCode: true }, () => {
          return Toast.show({ text: "Kode baru gagal dikirim", type: "danger"});
        })
      })
    })
  }
}

const styles = StyleSheet.create({
  borderStyleHighLighted: {
    borderColor: "red",
  },
  underlineStyleBase: {
    width: 50,
    borderWidth: 1,
    backgroundColor: '#fff',
    borderRadius: 5,
    borderColor: '#707070',
    color: '#707070'
  },
  underlineStyleHighLighted: {
    borderColor: "#15aa1a",
  },
});


const mapStateToProps = (state) => ({
  
})

const mapDispatchToProps = {
  actionsetVerification: setVerification,
  actionresendVerification: resendVerification
}

export default connect(mapStateToProps, mapDispatchToProps)(Verification)
