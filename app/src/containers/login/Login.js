import React, { Component } from 'react'
import { View, Image } from 'react-native'
import { connect } from 'react-redux'
import { Container, Content, Form, Item, Input, Button, Text, Icon, Toast } from 'native-base';
import { reqLogin } from '../../../redux/actions/authAction';
import { global } from '../../styles';

class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _phoneNumber: '',
    }
  }

  render(){
    const { _phoneNumber } = this.state;
    return(
      <Container>
        <Content style={{backgroundColor: "#e9eff5"}} padder>
          <View style={{marginTop: 50}}>
            <View style={[global.centered, {flexDirection: 'row', marginBottom: 40}]}>
              <Image source={require("../../img/verif-icon.png")} style={{width: 80, height: 84}} />
              <View style={{marginLeft: 20, width: 200, justifyContent: 'center'}}>
                <Text style={{fontWeight: 'bold', color: '#15aa1a', marginBottom: 5}}>Silahkan Masukan Nomor Telepon Anda</Text>
                <Text note>Untuk Memulai</Text>
              </View>
            </View>

            <Form>
              <Item fixedLabel>
                <Icon active name='ios-phone-portrait' />
                <Text>+62</Text>
                <Input 
                  autoFocus={true}
                  ref={(c) => this._inputEmail = c}
                  keyboardType="phone-pad" 
                  returnKeyType="done" 
                  value={_phoneNumber}
                  placeholder="Nomor Telefon"
                  onChangeText={(e) => this.setState({ _phoneNumber: e })}
                  onSubmitEditing={() => this.onSubmit() }
                />
              </Item>
            </Form>
          </View>
        </Content>
        <View style={[global.container, {marginTop: 30}]}>
          <Button full rounded onPress={() => this.onSubmit()}>
            <Text>Kirim OTP</Text>
          </Button>

          <Button transparent full onPress={() => this.goToTerms()}>
            <Text>Syarat & Ketentuan</Text>
          </Button>
        </View>
      </Container>
    )
  }


  goToTerms(){
    const { navigation } = this.props;
    return navigation.navigate('TermConditionPage');
  }

  onSubmit(){
    const { _phoneNumber } = this.state;
    var _regex = /^[(]{0,1}[0-9]{1,4}[)]{0,1}[0-9]*$/g;
    if(_phoneNumber){
      if((_phoneNumber.length >= 9 && _phoneNumber.length <= 16) && _phoneNumber.match(_regex) ){
        var nPhone = _phoneNumber.replace(/^(^\+62\s?|^0|^62)|\s/g, "");
        return this.onLogin(nPhone);
      }else{
        return Toast.show({ text: "No telepon tidak sesuai", type: "danger"});
      }
    }else{
      return Toast.show({ text: "Form tidak boleh kosong", type: "danger"});
    }
  }

  onLogin(phone) {
    const { actionreqLogin, navigation } = this.props;
    var data = { "phoneNumber": phone }
    Toast.show({ text: "Harap Tunggu"});
    return this.setState({_modalVisible: false, _btnLoading: true}, () => {
      return actionreqLogin(data, (vcid) => {
        return this.setState({_btnLoading: false}, () => {
          return navigation.navigate('VerificationPage', {
            data: {
              vcid: vcid,
              value: phone
            },
            from: 'login'
          });
        })
      } , () => {
        return this.setState({_btnLoading: false}, () => {
          return Toast.show({ text: "Login Failed", type: "danger"});
        })
      });
    })
  }
}

const mapStateToProps = (state) => ({
})

const mapDispatchToProps = {
  actionreqLogin: reqLogin
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);