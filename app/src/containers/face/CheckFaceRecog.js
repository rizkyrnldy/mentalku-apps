import React, { Component } from 'react'
import { View, Dimensions, Vibration, Image } from 'react-native';
import { global } from '../../styles'
import { Container, Text, Header, Icon, Left, Body, Title, Button, Right } from 'native-base';
import * as FaceDetector from 'expo-face-detector';
import { Camera } from 'expo-camera';
import update from 'react-addons-update';
import { StatusBar } from 'expo-status-bar';
import * as ImageManipulator from "expo-image-manipulator";

export default class CheckFaceRecog extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  handleFacesDetected = (a) => {
    const { onSubmit } = this.props;
    if(a.faces.length > 0){
      this.takePicture((res) => {
        // Vibration.vibrate();
        return onSubmit(res);
      })
    }
  }

  takePicture = async (cb) => {  
    let photo = await this.camera.takePictureAsync({ quality: 0, base64: true });
    let resizedPhoto = await ImageManipulator.manipulateAsync(
      photo.uri,
      [{ resize: { width: 300, height: 300 } }],
      { compress: 1, format: "jpeg", base64: true }
    );
    const { base64 } = resizedPhoto;
    let img = `data:image/jpg;base64,${base64}`;
    return cb( img )
  }

  render() {
    const { onCloseModal } = this.props
    const { width } = Dimensions.get('screen');
    return (
      <Container>
        <Header>
          <Left>
            <Button transparent onPress={() => onCloseModal()}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Validasi Face</Title>
          </Body>
          <Right />
        </Header>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000'}}>
          <View style={[{ width: width, height: width }]}>
            <Camera 
              ref={ref => { this.camera = ref }}
              style={{ flex: 1 }} 
              type={Camera.Constants.Type.front}
              ratio={"1:1"}
              autoFocus={true}
              onFacesDetected={this.handleFacesDetected}
              faceDetectorSettings={{
                mode: FaceDetector.Constants.Mode.fast,
                detectLandmarks: FaceDetector.Constants.Landmarks.none,
                runClassifications: FaceDetector.Constants.Classifications.none,
                minDetectionInterval: 3000,
                tracking: true,
              }}
            >
              <View style={[global.centered, {flex: 1, backgroundColor: 'transparent', }]}>
                <View style={[global.justify, { width: width-50, height: width-90, backgroundColor: 'transparent'}]}>
                  <View>
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                    <View style={{backgroundColor: 'green', height: 20, width: 2}} />
                  </View>
                  <View>
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                    <View style={{backgroundColor: 'green', height: 20, width: 2, alignSelf: 'flex-end'}} />
                  </View>
                </View>
                
                <View style={[global.justify, { width: width-50 }]}>
                  <View>
                    <View style={{backgroundColor: 'green', height: 20, width: 2}} />
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                  </View>
                  <View>
                    <View style={{backgroundColor: 'green', height: 20, width: 2, alignSelf: 'flex-end'}} />
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                  </View>
                </View>
              </View>
              
            </Camera>
          </View>
          
        </View>
      </Container>
    )
  }

}