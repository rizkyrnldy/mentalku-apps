import React, { Component } from 'react'
import { View, Dimensions, Vibration, Image } from 'react-native';
import { global } from '../../styles'
import { Container, Text, Header, Icon, Left, Body, Right, Title, Button, } from 'native-base';
import * as FaceDetector from 'expo-face-detector';
import { Camera } from 'expo-camera';
import update from 'react-addons-update';
import { StatusBar } from 'expo-status-bar';
import * as ImageManipulator from "expo-image-manipulator";

export default class RegistFaceRecog extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
       _faces: [],
       _intervalId: null,
       _currentCount: 3,
       _avatar: null
    }
  }

  // componentDidMount(){
  //   var intervalId = setInterval(this.timer, 1000);
  //   return this.setState({ _intervalId: intervalId });
  // }

  // componentWillUnmount(){
  //   return clearInterval(this.state._intervalId);
  // }
  
  // timer = () => {
  //   var newCount = this.state._currentCount - 1;
  //   if(newCount >= 0) { 
  //     return this.setState({ _currentCount: newCount });
  //   } else {

  //     return clearInterval(this.state._intervalId);
  //   }
  // }
  
  handleFacesDetected = (a) => {
    const { onSubmit } = this.props;
    if(a.faces.length > 0){
      this.takePicture((res) => {
        // Vibration.vibrate();
        return this.setState({ _faces: update(this.state._faces, { $push: [res] }) }, () => {
          return this.state._faces.length === 3 && onSubmit(this.state._faces)
        });
      })
    }
  }

  takePicture = async (cb) => {  
    let photo = await this.camera.takePictureAsync({ quality: 0 });
    let resizedPhoto = await ImageManipulator.manipulateAsync(
      photo.uri,
      [{ resize: { width: 300, height: 300 } }],
      { compress: 1, format: "jpeg", base64: true }
    );
    const { base64 } = resizedPhoto;
    let img = `data:image/jpg;base64,${base64}`;
    this.setState({
      _avatar: img
    })
    return cb( img )
  }

  render() {
    const { onCloseModal } = this.props
    const { width } = Dimensions.get('screen');
    return (
      <Container>
        <Header searchBar rounded>
          <Left>
            <Button transparent onPress={() => this.setState({ _faces: [] }, () => onCloseModal())}>
              <Icon name='arrow-back' />
            </Button>
          </Left>
          <Body>
            <Title>Register Face</Title>
          </Body>
          <Right />
        </Header>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#000'}}>
          <View style={[{ width: width, height: width }]}>
            <Camera 
              ref={ref => { this.camera = ref }}
              style={{ flex: 1 }} 
              type={Camera.Constants.Type.front}
              ratio={"1:1"}
              autoFocus={true}
              onFacesDetected={this.handleFacesDetected}
              faceDetectorSettings={{
                mode: FaceDetector.Constants.Mode.fast,
                detectLandmarks: FaceDetector.Constants.Landmarks.none,
                runClassifications: FaceDetector.Constants.Classifications.none,
                minDetectionInterval: 2000,
                tracking: true,
              }}
            >
              <View style={[global.centered, {flex: 1, backgroundColor: 'transparent', }]}>
                <View style={[global.justify, { width: width-50, height: width-90, backgroundColor: 'transparent'}]}>
                  <View>
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                    <View style={{backgroundColor: 'green', height: 20, width: 2}} />
                  </View>
                  <View>
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                    <View style={{backgroundColor: 'green', height: 20, width: 2, alignSelf: 'flex-end'}} />
                  </View>
                </View>
                
                <View style={[global.justify, { width: width-50 }]}>
                  <View>
                    <View style={{backgroundColor: 'green', height: 20, width: 2}} />
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                  </View>
                  <View>
                    <View style={{backgroundColor: 'green', height: 20, width: 2, alignSelf: 'flex-end'}} />
                    <View style={{backgroundColor: 'green', height: 2, width: 20}} />
                  </View>
                </View>
              </View>
              
            </Camera>
            <View style={{position: 'absolute', bottom: -40, left: 0, right: 0}}>
              <Text style={{color: '#fff', textAlign: 'center'}}>
                Total Capture: {this.state._faces.length}
              </Text>

              {this.state._avatar ?
                <Image 
                  source={{uri: this.state._avatar}}
                  style={{width: 100, height: 100}}
                /> : null
              }
            </View>
          </View>
          
        </View>
      </Container>
    )
  }

}