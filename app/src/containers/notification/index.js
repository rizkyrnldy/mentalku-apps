import React, { Component } from 'react'
import { View, RefreshControl, Dimensions } from 'react-native'
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, H3, Spinner } from 'native-base';
import { connect } from 'react-redux'
import { Header, Loading, Empty } from '../../components'
import { unmountListNotification, setListNotification, setLoadMoreNotification } from '../../../redux/actions/notification/notificationAction'
import moment from 'moment';
const { height } = Dimensions.get('window')

export class index extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _current: 1,
      _type: 'created_at',
      _orderBy: 'desc',
      _category: 'NOTIFICATION',
      _loading: false
    }
  }

  componentDidMount(){
    return this.getData();
  }

  getData(){
    const { actionsetListNotification } = this.props;
    const { _current, _type, _orderBy, _category } = this.state;
    var payload = {
      current: _current,
      type: _type,
      orderBy: _orderBy,
      category: _category
    }
    return actionsetListNotification(payload);
  }
  
  onRefresh(){
    this.setState({ _loading: true, _current: 0 }, () => {
      this.setState({ _loading: false })
      return this.getData();
    })
  }

  setCurrentReadOffset = (event) => {
    const { actionsetLoadMoreNotification } = this.props;
    let scrollHeight = event.nativeEvent.contentOffset.y + event.nativeEvent.layoutMeasurement.height;
    if(scrollHeight == event.nativeEvent.contentSize.height){
      return this.setState({ _current: this.state._current + 1, _loading: true }, () => {
        var value = { 
          current: this.state._current,
          type: this.state._type,
          orderBy: this.state._orderBy,
          category: this.state._category
        }
        return actionsetLoadMoreNotification(value, () => {
          this.setState({_loading: false})
        });
      })
      
    }
  }

  render() {
    const { getData: {data, loading} } = this.props;
    const { _loading } = this.state;
    if(loading){
      return <Loading />
    }
    return (
      <Container style={{backgroundColor: '#fff'}}>
        <Header show={true} title="Notifikasi"/> 
        <Content 
          refreshControl={<RefreshControl onRefresh={() => this.onRefresh()} />}
          scrollEventThrottle={300}
          onScroll={this.setCurrentReadOffset}
        >
          <List>
            {data.length > 0 ? data.map((res, i) => {
              return(
                <ListItem key={i}>
                  <Body>
                    <Text bold>{res.title}</Text>
                    <Text note style={{marginBottom: 15, marginTop: 5}}>{res.message}</Text>
                    <Text note>{moment(res.created_at).format('DD MMMM YYYY H:m:s')}</Text>
                  </Body>
                </ListItem>
              )
            }): <Empty text="Tidak ada notifikasi"/>}
          </List>
          {_loading && <Spinner />}
        </Content>
      </Container>
    )
  }

  componentWillUnmount(){
    const { actionunmountListNotification } = this.props;
    return actionunmountListNotification()
  }
}

const mapStateToProps = (state) => ({
  getData: state.notification.list
})

const mapDispatchToProps = {
  actionsetListNotification: setListNotification,
  actionsetLoadMoreNotification: setLoadMoreNotification,
  actionunmountListNotification: unmountListNotification
}

export default connect(mapStateToProps, mapDispatchToProps)(index)
