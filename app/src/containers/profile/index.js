import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Image, RefreshControl } from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Text, Icon, Left, Body, Right, H3 } from 'native-base';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { global } from '../../styles';
import { Header, Loading } from '../../components';
import { setProfile } from '../../../redux/actions/profile/profileAction';

export class Profile extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentDidMount() {
    const { actionsetProfile } = this.props;
    return actionsetProfile()
  }
  
  render() {
    const { getdata: { loading, data }, getAuth: { dataUser }, navigation } = this.props;
    if(loading){
      return <Loading />
    }
    return (
      <Container>
        <Header show={false} />
        <Content>
          <View>
            <Image source={require('../../img/bg-profile.png')} style={{ width: null, height: null, aspectRatio: 1, backgroundColor: '#fff', borderRadius: 15, overflow: 'hidden'}} />
            <View style={{position: 'absolute', left: 0, right: 0, top: 50, alignItems: 'center'}}>
              <Image source={require('../../img/logo-icon-dark.png')} style={{ width: 60, height: 77 }} />
            </View>
          </View>
          <View style={[global.container, { position: 'relative', marginTop: -80  }]}>
            <Card style={{flex: 0, borderRadius: 20, overflow: 'hidden'}}>
              <CardItem style={{ borderRadius: 60, height: 100 }}>
                <Left>
                  <Thumbnail 
                    source={!data.avatar ? require('../../img/avatar-default.png') : {uri: data.avatar}} 
                  />
                  <Body>
                    <H3>Hi! {data.fullName ? data.fullName : data.phoneNumber}</H3>
                    {/* <Text note>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</Text> */}
                  </Body>
                </Left>
              </CardItem>
            </Card>

            <Card style={{ borderRadius: 20, overflow: 'hidden' }}>
              <TouchableNativeFeedback onPress={() => navigation.navigate('EditProfileInformationPage', {
                data: data
              })}>
                <CardItem>
                  <View style={{marginRight: 20, backgroundColor: '#15aa1a', alignItems: 'center', justifyContent: 'center', height: 40, width: 40, borderRadius: 100, overflow: 'hidden' }}>
                    {/* <Icon active name="ios-lock" style={{ color: '#fff', textAlign: 'center' }} /> */}
                    <Icon type="AntDesign" name="user" style={{color: '#fff', fontSize: 24, textAlign: 'center'}} />
                  </View>
                  <Body style={{flex: 1}}>
                    <H3 style={{fontSize: 18}}>Profile Information</H3>
                    <Text note>Identitas profil anda</Text>
                  </Body>

                  <Right style={{flex: 0.2 }}>
                    <View style={{flexDirection: 'row'}}>
                      {dataUser.strength.includes('contacts') && <Icon name="ios-alert" style={{marginRight: 10, color: '#ff0000'}}/> }
                      <Icon name="ios-arrow-forward" />
                    </View>
                  </Right>
                </CardItem>
                </TouchableNativeFeedback>
            </Card>

            <Card style={{  borderRadius: 20, overflow: 'hidden' }}>
              <TouchableNativeFeedback onPress={() => navigation.navigate('EditProfileFaceRecogPage', {
                data: data
              })}>
                <CardItem>
                  <View style={{marginRight: 20, backgroundColor: '#15aa1a', alignItems: 'center', justifyContent: 'center', height: 40, width: 40, borderRadius: 100, overflow: 'hidden' }}>
                    <Icon type="MaterialCommunityIcons" name="face-recognition" style={{color: '#fff', fontSize: 24, textAlign: 'center'}} />
                  </View>
                  <View style={{flex: 1}}>
                    <H3 style={{fontSize: 18}}>Face Recognition</H3>
                    <Text note>Validasi aplikasi menggunakan Face Recognition</Text>
                  </View>
                  <Right style={{flex: 0.2 }}>
                    <View style={{flexDirection: 'row'}}>
                      {dataUser.strength.includes('faceRecog') && <Icon name="ios-alert" style={{marginRight: 10, color: '#ff0000'}}/> }
                      <Icon name="ios-arrow-forward" />
                    </View>
                  </Right>
                </CardItem>
              </TouchableNativeFeedback>
            </Card>
            
            
          </View>  
        </Content>
      </Container>
    )
  }

}

const mapStateToProps = (state) => ({
  getAuth: state.auth,
  getdata: state.profile,
})

const mapDispatchToProps = {
  actionsetProfile: setProfile
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
