import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ToastAndroid, View, Image, Modal,  } from 'react-native';
import { Container, Content, Card, Body,  CardItem, Text, Button } from 'native-base';
import { Camera } from 'expo-camera';
import { registerFace } from '../../../../redux/actions/face/faceRecogAction'
import { global } from '../../../styles';
import RegistFaceRecog from '../../face/RegistFaceRecog';
import { setProfile } from '../../../../redux/actions/profile/profileAction';

export class EditProfileFaceRecog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _btnLoading: false,
      _modalVisible: false,
      _hasCameraPermission: null,
    }
  }

  // componentDidMount() {
  //   const { actionsetProfile } = this.props;
  //   return actionsetProfile()
  // }
  
  render() {
    const { getdata: { data }, getAuth: {dataUser} } = this.props;
    const { _btnLoading, _modalVisible } = this.state;
    return (
      <Container>
        <Content padder>
          <Card style={{ borderRadius: 20, overflow: 'hidden', marginBottom: 10}}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  TATA CARA PEMBUATAN FACE RECOG
                </Text>
                <Text note>1. TIDAK BOLEH BANYAK BERGERAK</Text>
                <Text note>2. MUKA LURUS MENGHADAP KAMERA</Text>
                <Text note>3. MUKA TIDAK BISA LEBIH DARI 1</Text>
              </Body>
            </CardItem>
          </Card>
          
          <Card style={{ borderRadius: 20, overflow: 'hidden', marginBottom: 40}}>
            <CardItem>
              <Body>
                <Text style={{backgroundColor: '#fff',color: '#062b54', fontWeight: 'bold', marginBottom: 10}}>
                  {data.userFace.length !== 3  ? "CONTOH FACE RECOG" : "HASIL FACE RECOG"}
                </Text>
                {
                  data.userFace.length !== 3  ? 
                    <Image 
                      source={require('../../../img/face/face_tutorial.png')} 
                      style={{ width: 250, height: 83, alignSelf: 'center'}} 
                    /> : 
                    <View style={{flexDirection: 'row', width: '100%', justifyContent: 'space-between'}}>
                      {data.userFace.map((res, i) => {
                        return(
                          <Image 
                            key={i}
                            source={{uri: res}} 
                            style={{ width: 80, height: 80}} 
                          />
                        )
                      })}
                    </View>
                }
              </Body>
            </CardItem>
          </Card>

        </Content>
        
        {data.userFace.length === 0 ?
          <View style={[global.container]}>
            <Button full rounded onPress={() => this.openModal()} disabled={_btnLoading}>
              <Text>Mulai</Text>
            </Button>
          </View> : null
        }

        <Modal
          animationType="slide"
          transparent={true}
          visible={_modalVisible}
          onRequestClose={() => this.onCloseModal()}>
            <RegistFaceRecog 
              passState={this.state}
              passProps={this.props}
              onCloseModal={() => this.onCloseModal()}
              onSubmit={(img) => this.onSubmit(img)} 
            />
        </Modal>

      </Container>
    )
  }

  async openModal(){
    // const { status } = await Permissions.askAsync(Permissions.CAMERA);
    const { status } = await Camera.requestPermissionsAsync();
    if(status === 'granted'){
      return this.setState({
        _hasCameraPermission: status === 'granted',
        _modalVisible: true,
        _btnLoading: true
      });
    }else{
      return ToastAndroid.show("Permission kamera tidak di izinkan", ToastAndroid.SHORT);
    }
  }

  onCloseModal(){
    return this.setState({ _btnLoading: false, _modalVisible: false })
  }

  onSubmit(img){
    const { actionregisterFace, navigation } = this.props;
    return this.setState({ _modalVisible: false }, () => {
      ToastAndroid.show("Harap Tunggu", ToastAndroid.SHORT);
      var payload = { images: img }
      return actionregisterFace(payload, (res) => {
        navigation.goBack();
        return this.setState({ _btnLoading: false }, () => ToastAndroid.show(res.message, ToastAndroid.SHORT))
      }, (err) => {
        return this.setState({ _btnLoading: false }, () => ToastAndroid.show(err.message, ToastAndroid.SHORT))
      })
    })
  }

}

const mapStateToProps = (state) => ({
  getAuth: state.auth,
  getdata: state.profile,
})

const mapDispatchToProps = {
  actionregisterFace: registerFace,
  actionsetProfile: setProfile
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileFaceRecog)
