import React, { Component } from 'react'
import { connect } from 'react-redux'
import { View, Image, RefreshControl, Dimensions, TouchableOpacity, Modal } from 'react-native';
import { Container, Content, Card, Form, Label, Input, Item,  CardItem, Thumbnail, Text, Button, Textarea, Icon, Picker, Toast } from 'native-base';
import { validationForm } from '../../../helper';
import { Loading, Textinput, Camera as Cameras } from '../../../components';
import { editProfileInformation, unmountProfile } from '../../../../redux/actions/profile/profileAction';
import { unmoutListDataMaster } from '../../../../redux/actions/component/dataMasterAction'
import update from 'react-addons-update';
import * as ImagePicker from 'expo-image-picker';
import { Camera } from 'expo-camera';
import * as ImageManipulator from "expo-image-manipulator";
const { width, height } = Dimensions.get('window')

export class EditProfileInformation extends Component {
  constructor(props) {
    super(props)
    this.state = {
      _form: [],
      _avatar: null,
      _avatarEdited: false,
      
      _ktp: null,
      _ktpEdited: false,

      _loading: true,
      _btnLoading: false,

      _modalVisible: false,
    }
  }

  componentDidMount() {
    const { route: { params } } = this.props;
    // console.log(444, params.data);
    var label = params.data.user_contacts.district ? `${params.data.user_contacts.district.name}, ${params.data.user_contacts.district.regencies.name}, ${params.data.user_contacts.district.regencies.provinces.name}` : '';
    var value = params.data.user_contacts.district ? params.data.user_contacts.district.id : '';
    return this.setState({
      _form: [
        {name: 'nik', title: 'NIK', value: params.data.nik, error: false, capital: "words", required: true, keyboardType: "numeric"},
        // {name: 'nik', title: 'NIK', value: '3174010506920007', error: false, capital: "words", required: true, keyboardType: "numeric"},
        {name: 'fullName', title: 'Nama Lengkap', value: params.data.fullName, error: false, capital: "words", required: true},
        {name: 'email', title: 'Email', value: params.data.email, error: false, required: true},
        // {name: 'email', title: 'Email', value: 'pasuruan.dsoetomo@gmail.com', error: false, required: true},
        {name: 'dateOfBirth', title: 'Tanggal Lahir', value: params.data.user_contacts.dateOfBirth, type: 'datePicker', error: false, required: true},
        {name: 'domicile', title: 'Domisili', value: { "label": label, "value": value }, type: 'autocomplete', error: false,  required: true},
        {name: 'address', title: 'Alamat Lengkap', value: params.data.user_contacts.address, type: 'textArea', error: false,  required: true},
        {name: 'gender', title: 'Jenis Kelamin', value: params.data.user_contacts.gender, type: 'picker', error: false, required: true},
        {name: 'profession', title: 'Status Pekerjaan', value: params.data.user_contacts.profession, error: false,  required: true},
      ],
      _avatar: params.data.avatar,
      _ktp: params.data.ktp,
      _loading: false
    })
  }
  

  render() {
    const { _form, _loading, _btnLoading, _avatar, _ktp, _modalVisible } = this.state;
    if(_loading){
      return <Loading />
    }
    return (
      <Container>
        <Content padder
          refreshControl={<RefreshControl refreshing={_btnLoading} />}
        >
          <Card style={{ borderRadius: 20, paddingBottom: 20, marginBottom: 40 }}>
            <CardItem style={{flexDirection: 'column'}}>
              <View style={{flex: 1, marginTop: 20, marginBottom: 40}}>
                <Button transparent onPress={() => this.pickImage()}>
                  <Thumbnail 
                    large
                    source={_avatar === null ? require('../../../img/avatar-default.png') : {uri: _avatar}} 
                  />
                </Button>
              </View>
              <Form style={{width: '100%'}}>
                <Textinput 
                  data={_form} 
                  onChangeText={(e, i) => this.setState({
                    _form: update(this.state._form, { 
                      [i]: { value: { $set: e }  }
                    }) 
                  })}
                />
              </Form>
              <View style={{ flex: 1, alignSelf: 'stretch', }}>
                <Label style={{flex: 1, marginBottom: 10, fontSize: 14, fontWeight: 'bold'}}>Foto KTP:</Label>
                <TouchableOpacity onPress={() => this.launchCamera()} >
                  {_ktp === null ? 
                  <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#ddd', alignItems: 'center', justifyContent: 'center', width: '100%', height: 120, borderRadius: 10}}>
                    <Icon type="AntDesign" name="camerao" style={{color: '#333', fontSize: 42, textAlign: 'center'}} />                    
                  </View> :
                  <Image 
                    source={{uri: _ktp}} 
                    style={{ width: '100%', height: 200, borderRadius: 10, alignSelf: 'center' }} 
                    onPress={() => this.launchCamera()} 
                  />}
                </TouchableOpacity>
                <Text note style={{fontStyle: 'italic', fontSize: 12, marginTop: 5}}>
                  *Foto KTP harus jelas
                </Text>
              </View>
            </CardItem>
          </Card>
          <Button full rounded onPress={() => this.onSubmit()} disabled={_btnLoading}>
            <Text>ubah profil</Text>
          </Button>
        </Content>

        <Modal
          animationType="slide"
          transparent={true}
          visible={_modalVisible}
          onRequestClose={() => this.setState({_modalVisible: false}) }>
            <Cameras 
              passState={this.state}
              passProps={this.props}
              ratio="3:2"
              onCloseModal={() => this.setState({_modalVisible: false}) }
              onSubmit={uri => this.setState({ _modalVisible: false, _ktpEdited: true, _ktp: uri })} 
            />
        </Modal>

      </Container>
    )
  }

  async pickImage(){
    // const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
    const { status } = await Camera.requestPermissionsAsync();
    if (status !== 'granted') {
      return Toast.show({ text: "Maaf, kita membutuhkan permission", type: "danger" });
    }else{
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        aspect: [1, 1],
        quality: 1,
      })
      let resizedPhoto = await ImageManipulator.manipulateAsync(
        result.uri,
        [{ resize: { width: 600, height: 600 } }],
        { compress: 1 }
      );
      if (!result.cancelled) {
        return this.setState({
          '_avatarEdited': true,
          '_avatar': resizedPhoto.uri,
        })
      }
    }
  };

  async launchCamera(){
    // const { status } = await Permissions.askAsync(Permissions.CAMERA, Permissions.CAMERA_ROLL);
    const { status } = await Camera.requestPermissionsAsync();
    if (status !== 'granted') {
      return Toast.show({ text: "Maaf, kita membutuhkan permission", type: "danger" });
    }else{
      return this.setState({
        _modalVisible: true,
      })
    }
  }

  onSubmit(){
    const { navigation, actioneditProfileInformation } = this.props;
    const { _form, _avatar, _avatarEdited, _ktp, _ktpEdited } = this.state;
    return this.setState({_btnLoading: true}, () => {
      return validationForm(_form, (res) => {
        if(res.nik.length === 16){
          var newRes = res;
          if(_avatarEdited){
            newRes.avatar = _avatar;
          }
          if(_ktpEdited){
            newRes.ktp = _ktp;
          }
          return actioneditProfileInformation(res, () => {
            this.setState({_btnLoading: false}, () => {
              navigation.goBack();
              return Toast.show({ text: "Ubah Profil Sukses", type: "success" });
            })
          }, (err) => {
            var nForm = _form;
            var i = err === 'NIK Sudah Digunakan' ? 0 : 2;
            return this.setState({
              _form:  update(nForm, {
                [i]: { error: { $set: true } }
              }),
              _btnLoading: false
            }, () => {
              return Toast.show({ text: err ? err : 'Server Error', type: "danger" });
            })
          })
        }else{
          return this.setState({_btnLoading: false}, () => {
            return Toast.show({ text: "NIK KTP harus 16 digit", type: "danger" });
          })
        }
      }, (err)  => {
        return this.setState({
          _form: err,
          _btnLoading: false
        }, () => Toast.show({ text: "Form Tidak Boleh Kosong", type: "danger" }) )
      })
    })
  }

  componentWillUnmount(){
    const { actionunmoutListDataMaster } = this.props;
    return actionunmoutListDataMaster()
  }

}

const mapStateToProps = (state) => ({
  getAuth: state.auth,
})

const mapDispatchToProps = {
  actioneditProfileInformation: editProfileInformation,
  actionunmountProfile: unmountProfile,
  actionunmoutListDataMaster: unmoutListDataMaster
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfileInformation)
