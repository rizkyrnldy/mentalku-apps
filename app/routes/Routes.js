import React, { Component } from 'react'
import { Platform, Alert } from 'react-native';
import { connect } from 'react-redux'
import { checkAuth } from '../redux/actions/authAction';
import { registerFCM } from '../redux/actions/authAction';
import { setNotificationMerge } from '../redux/actions/notification/notificationAction';
import * as Containers from '../src/containers';
import { Loading } from '../src/components';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Tabbar from './Tabbar';
import Constants from 'expo-constants';
import * as Notifications from 'expo-notifications';
const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

export class Routes extends Component {
  constructor(props) {
    super(props)
    this.state = {
    }
    Notifications.addNotificationReceivedListener(this.setNotification);
    Notifications.addNotificationResponseReceivedListener(this.readNotification);
  }

  async componentDidMount() {
    const { actioncheckAuth, actionregisterFCM } = this.props;
    console.log(555, 'token');
    return registerForPushNotificationsAsync().then(token => {
      var payload = { fcm: token }
      return actionregisterFCM(payload, () => {
        return actioncheckAuth();
      })
    });
  }

  setNotification = (notification) => {
    const { actionsetNotificationMerge } = this.props;
    return actionsetNotificationMerge(notification);
  }

  readNotification = (a) => {
    // console.log(5551, a);
  }

  render() {
    const { getAuth: { loading, authed }} = this.props
    if(loading){  
      return <Loading />  
    }
    return (
      <NavigationContainer>
        <Stack.Navigator screenOptions={{ headerStyle: { backgroundColor: '#fff'}, headerTintColor: '#062b54', }}>
          { !authed ?  this.renderAuth() : this.renderPrivate() }
        </Stack.Navigator>
      </NavigationContainer>
    )
  }

  renderTabBar(){
    return(
      <Tab.Navigator tabBar={props => <Tabbar {...props} />}>
        <Tab.Screen name="HomePage" component={Containers.Home} options={{ tabBarLabel: 'Home', headerBackTitleVisible: false }}/>
        <Tab.Screen name="BillingPage" component={Containers.Billing} options={{ tabBarLabel: 'Riwayat' }}/>
        <Tab.Screen name="ProfilePage" component={Containers.Profile} options={{ tabBarLabel: 'Profil' }}/>
        <Tab.Screen name="NotificationPage" component={Containers.Notification} options={{ tabBarLabel: 'Notifikasi' }}/>
        <Tab.Screen name="MorePage" component={Containers.More} options={{ tabBarLabel: 'Lainnya' }}/>
      </Tab.Navigator>
    )
  }

  renderPrivate(){
    return(
      <>
        <Stack.Screen name="root" component={this.renderTabBar}  options={{ headerShown: false }} />
        
        <Stack.Screen name="EditProfileFaceRecogPage" component={Containers.EditProfileFaceRecog}  options={{ headerBackTitleVisible: false, title: 'Daftar Face Recog', headerBackTitleVisible: false}} />
        <Stack.Screen name="EditProfileInformationPage" component={Containers.EditProfileInformation}  options={{ headerBackTitleVisible: false, title: 'Edit Profile Information', headerBackTitleVisible: false }} />
        
        <Stack.Screen name="SimPage" component={Containers.Sim}  options={{ headerBackTitleVisible: false, title: '', headerBackTitleVisible: false }} />
        <Stack.Screen name="CreateSimSearchOutlet" component={Containers.CreateSimSearchOutlet}  options={{ headerBackTitleVisible: false, title: 'Cari Gerai', headerBackTitleVisible: false }} />
        <Stack.Screen name="CreateSimTypePricePage" component={Containers.CreateSimTypePrice}  options={{ headerBackTitleVisible: false, title: 'Tipe & Biaya Test Psikologi', headerBackTitleVisible: false }} />
        <Stack.Screen name="ConfirmationSimPage" component={Containers.ConfirmationSim}  options={{ headerBackTitleVisible: false, title: 'Konfirmasi', headerBackTitleVisible: false }} />
        
        <Stack.Screen name="ResultQuizPage" component={Containers.ResultQuiz}  options={{ headerShown: false, headerBackTitleVisible: false }} />
        
        <Stack.Screen name="ReaderPage" component={Containers.Reader}  options={{ headerBackTitleVisible: false, title: 'Reader SIM', headerBackTitleVisible: false }} />
        <Stack.Screen name="ReaderDetailPage" component={Containers.ReaderDetail}  options={{ headerBackTitleVisible: false, title: 'Reader SIM', headerBackTitleVisible: false }} />
        
        <Stack.Screen name="ListPsikologPage" component={Containers.ListPsikolog}  options={{ headerBackTitleVisible: false, title: 'List Psikolog', headerBackTitleVisible: false }} />
        <Stack.Screen name="DetailKonselingPage" component={Containers.DetailKonseling}  options={{ headerBackTitleVisible: false, title: 'Detail Psikolog', headerBackTitleVisible: false }} />
        <Stack.Screen name="ConfirmationKonselingPage" component={Containers.ConfirmationKonseling}  options={{ headerBackTitleVisible: false, title: 'Konfirmasi', headerBackTitleVisible: false }} />
        
        <Stack.Screen name="ListNewsPage" component={Containers.ListNews}  options={{ headerBackTitleVisible: false, title: 'List Artikel', headerBackTitleVisible: false }} />
        <Stack.Screen name="DetailNewsPage" component={Containers.DetailNews}  options={{ headerBackTitleVisible: false, title: 'Detail Artikel', headerBackTitleVisible: false }} />

        <Stack.Screen name="TermConditionPage" component={Containers.TermCondition}  options={{ headerBackTitleVisible: false, title: 'Syarat & Ketentuan', headerBackTitleVisible: false }}  />
        <Stack.Screen name="CertificateListPage" component={Containers.CertificateList}  options={{ headerBackTitleVisible: false, title: 'List Sertifikat', headerBackTitleVisible: false }}  />
        <Stack.Screen name="CertificateDetailPage" component={Containers.CertificateDetail}  options={{ headerBackTitleVisible: false, title: 'Detail Sertifikat', headerBackTitleVisible: false }}  />
        
        
        <Stack.Screen name="SupportPage" component={Containers.Support}  options={{ headerBackTitleVisible: false, title: 'Pusat Bantuan', headerBackTitleVisible: false }}  />

      </>
    )
  }
  
  renderAuth(){
    return(
      <>
        <Stack.Screen name="LandingPage" component={Containers.Landing} options={{ headerShown: false, headerBackTitleVisible: false, }} />
        <Stack.Screen name="LoginPage" component={Containers.Login} options={{ headerBackTitleVisible: false, title: 'Login', headerShown: true }} />
        <Stack.Screen name="VerificationPage" component={Containers.Verification}  options={{ headerBackTitleVisible: false, title: 'Verification' }}  />
        <Stack.Screen name="TermConditionPage" component={Containers.TermCondition}  options={{ headerBackTitleVisible: false, title: 'Syarat & Ketentuan' }}  />
      </>
    )
  }

  componentWillUnmount(){
    Notifications.removeNotificationSubscription(this.setNotification);
    Notifications.removeNotificationSubscription(this.readNotification);
  }

}

async function registerForPushNotificationsAsync() {
  // console.log(888, 'finalStatus')
  let token;
  if (Constants.isDevice) {
    const { status: existingStatus } = await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    console.log(888, finalStatus)
    if (existingStatus !== 'granted') {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
    }
    if (finalStatus !== 'granted') {
      // Alert.alert('Failed to get push token for push notification!');
      return;
    }
    token = (await Notifications.getExpoPushTokenAsync()).data;
    // console.log(999, token);
  } else {
    // Alert.alert('Must use physical device for Push Notifications');
  }
  if (Platform.OS === 'android') {
    Notifications.setNotificationChannelAsync('default', {
      name: 'default',
      importance: Notifications.AndroidImportance.MAX,
      vibrationPattern: [0, 250, 250, 250],
      lightColor: '#FF231F7C',
    });
  }

  return token;
}


const mapStateToProps = (state) => ({
  getAuth: state.auth
})

const mapDispatchToProps = {
  actioncheckAuth: checkAuth,
  
  actionregisterFCM: registerFCM,
  actionsetNotificationMerge: setNotificationMerge,
}

export default connect(mapStateToProps, mapDispatchToProps)(Routes)
